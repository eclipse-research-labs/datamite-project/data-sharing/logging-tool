# DATAMITE

## -Logging tool architecture

	Will be an indpepentent web service made on node.js, mysql.
	
	For authentication considering the option to be under keycloak (if will be used also by other datamite services)
	
## - An upper level description functionalities of the tool

	1.To log by saving on a database various actions of Datamite services like the requests made to get some dataset by a consumer.
	
	2.To log by saving on a database the creation, update, negociate action on an agreement(not only the action but store both the data agreement and its hash for validation).
	
	3.Since there will be a dashboard to visualise the logged data and provide various metrics and  filters, the logging service will have the appropiate methods to get these data on the dashboard.
	
	4.Depending the volume of the data and the requested format of them is possible the logged data to be also saved on database with various aggregated formats for quick access.
## - Architecture diagram
The architecture Diagram, as displayed below, presents the high-lever overview of the logging tool service's functions
within the DATAMITE system, in particular with the **Data sharing** Module. Focusing on logging various actions and data.

![Architecture diagram](logging/images/abtract_architecture.png "Architecture diagram")


## -Section: Requirements

Enable continuous monitoring of data usage to ensure compliance with defined rules and policies
The data sharing module should ensure integrity in the data sharing process
The data sharing module must provide a monitoring and auditing tool within the framework to transparently oversee transactions with external users, ensuring visibility and clarity in the handling of sensitive data sharing
The data sharing module should ensure integrity in the data sharing process
A transparent monitoring tool is essential to provide visibility into transactions with external users, fostering trust and clarity in data-sharing processes


## -Section: Swagger documentation

	https://datamite.api.iti.gr/docs/#/

Section: DEMO 

	https://drive.google.com/file/d/1GX4Fg2JxYOtB_oujD5QxOchSszWiAB9K/view?usp=drive_link
	
	
## - Indicative examples


    1.After a consumer requested some data, follows a request to the logging tool service to log this action
      HTTP Method: Post
      Endpoint: ‘https://loggingToolService/log/create/’ 
      Headers: Content-Type: application/json , userToken : 5as6dfg65th4h4ht4645
      
    Body
    {
      "endpoint": https://datamiteservice/asset/1/",

      "cοnsumerId": "1",

      "providerId": "2",

      "assetId"2",

      "aggreementId"42",

      "datetime"2024-02-18 23:55:44",

      "status"success",

      "type"data", 

      "message"Consumer X requested and got the asset data D....",

    }

    2.After the creation or update of an agreement we want to log this action but also store the agreement that was created/updated/success negociated etc.
      HTTP Method: Post
      Endpoint: ‘https://loggingToolService/log/createAgreement/’ 
      Headers: Content-Type: application/json , userToken : 5as6dfg65th4h4ht4645
      
    Body

    {
      
      "endpoint": https://datamiteservice/provider/createContract/",

      "providerId": "2",

      "aggreementId"42",

      "datetime"2024-02-18 23:55:44",

      "status"success",

      "type"agreement", 

      "policies": {
        "p1": "policy1",
        "p2": "policy2"
      }
      "message"Provider Z created an agreement....",
    }	

    3.In this exammple the logging service should provide all the requests for assets made by a specific consumer(id:2630) on a specific date range .

      HTTP Method: Get
      
      Endpoint: ‘https://loggingToolService/actionlog/getByDate?fromDate=2024-01-19T12:39:03.111Z&consumerId=2630&toDate=1/18/2025 9:58:24 PM/’ 
      
      Headers: Content-Type: application/json , userToken : 5as6dfg65th4h4ht4645

## - Section: Contacts
	Georgios Nikolaidis - geonikolai@iti.gr
	Vasilis Siopidis    - vasisiop@iti.gr
  
	
