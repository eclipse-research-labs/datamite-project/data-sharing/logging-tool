/**
 * Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * SPDX-License-Identifier: MIT
 * 
 * Contributors:
 * Georgios Nikolaidis - Author
 * Vasileios Siopidis - Coauthor
 * Konstantinos Votis - Coauthor
 */

import Joi from '@hapi/joi';
import moment from "moment";
import dbUtil from '../utils/db.util';
import { PolicyLog } from '../models/policyLog.model';
import { Logform } from 'winston';


const createValidator = Joi.object({
    providerId: Joi.string().required()
});

class PolicyLogService {
    async getAll() {
        const connection = await dbUtil.getDefaultConnection();
        const logs = await connection.getRepository(PolicyLog).find({
            where: {
                isDeleted: false
            }
        });
        return logs;
    }

    async getByDate(queryParams: any) {
        const connection = await dbUtil.getDefaultConnection();
        const query = connection.getRepository(PolicyLog).createQueryBuilder("m")
            .where("m.isDeleted = :isDeleted").setParameter("isDeleted", false)
            .orderBy("m.createdAt", "DESC");

        if (queryParams.fromDate !== 'undefined' && queryParams.fromDate) {
            query.andWhere("m.createdAt >= :fromDate").setParameter("fromDate", moment(queryParams.fromDate).startOf("d").toDate());
        }
        if (queryParams.toDate !== 'undefined' && queryParams.toDate) {
            query.andWhere("m.createdAt <= :toDate").setParameter("toDate", moment(queryParams.toDate).endOf("d").toDate());
        }
        if (queryParams.userId) {
            query.andWhere("m.userId = :userId").setParameter("userId", queryParams.userId);
        }
        const logs = await query.getMany();
        return logs;
    }

    async getById(id: number) {
        const connection = await dbUtil.getDefaultConnection();
        const log = await connection.getRepository(PolicyLog).findOne({ where: { id } });
        return log;
    }

    async getByPolicyId(queryParams: any) {
        const connection = await dbUtil.getDefaultConnection();
        const query = connection.getRepository(PolicyLog).createQueryBuilder("m")
            .orderBy("m.createdAt", "DESC");
        if (queryParams.policyId) {
            query.andWhere("m.policyId = :policyId").setParameter("policyId", queryParams.policyId);
        }
        const logs = await query.getMany();
        return logs;
    }

    async create(model: any) {
        const validatedModel = await createValidator.validateAsync(model, { allowUnknown: true });
        const connection = await dbUtil.getDefaultConnection();
        const log = new PolicyLog();
        Object.assign(log, validatedModel);
        const newLog = await connection.manager.save(log);
        return newLog;
    }

    async update(model: any) {
        const validatedModel = await createValidator.validateAsync(model, { allowUnknown: true });
        const connection = await dbUtil.getDefaultConnection();
        let PolicyId = model.PolicyId;
        const query = connection.getRepository(PolicyLog).createQueryBuilder("m")
            .where("m.isDeleted = :isDeleted").setParameter("isDeleted", false)
            .orderBy("m.createdAt", "DESC");
        query.andWhere("m.PolicyId").setParameter("PolicyId", PolicyId);
        const asetLog = await query.getOneOrFail();
        let creatAt = asetLog.createdAt;
        Object.assign(asetLog, validatedModel);
        asetLog.createdAt = creatAt;
        const newLog = await connection.manager.save(asetLog);
        return newLog;
    }

    async transfer(model: any) {
        const validatedModel = await createValidator.validateAsync(model, { allowUnknown: true });
        const connection = await dbUtil.getDefaultConnection();
        const log = new PolicyLog();
        Object.assign(log, validatedModel);
        const newLog = await connection.manager.save(log);
        return newLog;
    }

    async createRandom() {

        const connection = await dbUtil.getDefaultConnection();
        const log = new PolicyLog();
        log.userId = await this.getRandomInt(1, 15000).toString();
        log.assetId = '350ba92e-7658-4816-b6f6-579529d1cc7f';
        log.policyId = '880ba92e-7658-4816-b6f6-579529d1cc99';
        log.createdAt = new Date();
        const newLog = await connection.manager.save(log);
        return newLog;
    }

    async getRandomInt(min: number, max: number) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min) + min);
    }

    async createPolicyLog(model: any, action: string) {

        const connection = await dbUtil.getDefaultConnection();
        const log = new PolicyLog();
        log.userId = model.userId;
        log.action = action;
        log.assetId = model.assetId;
        log.policyId = model.policyId;
        // if (!model.assetId) {
        //     log.assetId = '350ba92e-7658-4816-b6f6-579529d1cc7f';
        // }
        // else {
        //     log.assetId = model.assetId;
        // }
        // if (!model.policyId) {
        //     log.policyId = '880ba92e-7658-4816-b6f6-579529d1cc99';
        // }
        // else {
        //     log.policyId = model.policyId;
        // }
        log.isDeleted = model.isDeleted;
        if (action === 'delete') {
            log.isDeleted = true;
        }
        log.aggreementId = model.aggreementId;
        log.cοnsumerId = model.consumerId;
        log.endpointUsed = model.endpointUsed;
        log.providerId = model.providerId;
        log.createdAt = model.createdAt;
        log.assetName = model.assetName;
        log.userName = model.userName;
        log.cοnsumerName = model.cοnsumerName;
        log.providerName = model.providerName;
        log.type = model.type;
        log.status = model.status;
        if (model.obligations) {
            log.obligations = JSON.stringify(model.obligations);
        } else {
            log.obligations = "[]";
        }
        if (model.prohibitions) {
            log.prohibitions = JSON.stringify(model.prohibitions);
        } else {
            log.prohibitions = "[]";
        } if (model.permissions) {
            log.permissions = JSON.stringify(model.permissions);
        } else {
            log.permissions = "[]";
        }
        log.assigner = model.assigner;
        log.assignee = model.assignee;
        log.ipAddress = model.ipAddress;
        const newLog = await connection.manager.save(log);
        return newLog;
    }
}

export default new PolicyLogService();