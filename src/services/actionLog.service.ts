/**
 * Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * SPDX-License-Identifier: MIT
 * 
 * Contributors:
 * Georgios Nikolaidis - Author
 * Vasileios Siopidis - Coauthor
 * Konstantinos Votis - Coauthor
 */

import Joi from '@hapi/joi';
import moment from "moment";
import dbUtil from '../utils/db.util';
import { ActionLog } from '../models/actionLog.model';

const createValidator = Joi.object({
    providerId: Joi.string().required()
});

class ActionLogService {
    async getAll() {
        const connection = await dbUtil.getDefaultConnection();
        const logs = await connection.getRepository(ActionLog).find({
            where: {
                isDeleted: false
            }
        });

        return logs;
    }

    async getByDate(queryParams: any) {
        const connection = await dbUtil.getDefaultConnection();
        const query = connection.getRepository(ActionLog).createQueryBuilder("m")
            .where("m.isDeleted = :isDeleted").setParameter("isDeleted", false)
            .orderBy("m.createdAt", "DESC");

        if (queryParams.fromDate) {
            query.andWhere("m.createdAt >= :fromDate").setParameter("fromDate", moment(queryParams.fromDate).startOf("d").toDate());
        } else {
            query.andWhere("m.createdAt >= :fromDate").setParameter("fromDate", moment().startOf("year").toDate());
        }
        if (queryParams.toDate) {
            query.andWhere("m.createdAt <= :toDate").setParameter("toDate", moment(queryParams.toDate).endOf("d").toDate());
        } else {
            query.andWhere("m.createdAt <= :toDate").setParameter("toDate", moment().endOf("day").toDate());
        }
        if (queryParams.userId) {
            query.andWhere("m.userId = :userId").setParameter("userId", queryParams.userId);
        }
        const logs = await query.getMany();
        return logs;
    }

    async getById(id: any) {
        let dd = 4;
        const connection = await dbUtil.getDefaultConnection();
        const log = await connection.getRepository(ActionLog).findOne({ where: { id } });
        return log;
    }

    async create(model: any) {
        const validatedModel = await createValidator.validateAsync(model, { allowUnknown: true });
        const connection = await dbUtil.getDefaultConnection();
        const actionLog = new ActionLog();
        Object.assign(actionLog, validatedModel);
        actionLog.createdAt = new Date();
        const newLog = await connection.manager.save(actionLog);
        return newLog;
    }

    async createRandom() {
        const connection = await dbUtil.getDefaultConnection();
        const actionLog = new ActionLog();
        actionLog.userId = (await this.getRandomInt(1, 15000)).toString();
        actionLog.createdAt = new Date();
        const newLog = await connection.manager.save(actionLog);
        return newLog;
    }

    async createLog(model: any, action: string) {
        const connection = await dbUtil.getDefaultConnection();
        const actionLog = new ActionLog();
        actionLog.userId = model.userId;
        actionLog.action = action;
        actionLog.createdAt = model.createdAt;
        const newLog = await connection.manager.save(actionLog);
        return newLog;
    }

    async getRandomInt(min: number, max: number) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min) + min);
    }
}

export default new ActionLogService();