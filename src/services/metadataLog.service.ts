/**
 * Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * SPDX-License-Identifier: MIT
 * 
 * Contributors:
 * Georgios Nikolaidis - Author
 * Vasileios Siopidis - Coauthor
 * Konstantinos Votis - Coauthor
 */

import Joi from '@hapi/joi';
import moment from "moment";
import dbUtil from '../utils/db.util';
import { MetadataLog } from '../models/metadataLog.model';
import { Logform } from 'winston';


const createValidator = Joi.object({
    catalogId: Joi.string().required()
});

class MetadataLogService {
    async getAll() {
        const connection = await dbUtil.getDefaultConnection();
        const logs = await connection.getRepository(MetadataLog).find({
            where: {
                isDeleted: false
            }
        });
        return logs;
    }

    async getByDate(queryParams: any) {
        const connection = await dbUtil.getDefaultConnection();
        const query = connection.getRepository(MetadataLog).createQueryBuilder("m")
            .where("m.isDeleted = :isDeleted").setParameter("isDeleted", false)
            .orderBy("m.createdAt", "DESC");

        if (queryParams.fromDate) {
            query.andWhere("m.createdAt >= :fromDate").setParameter("fromDate", moment(queryParams.fromDate).startOf("d").toDate());
        } else {
            query.andWhere("m.createdAt >= :fromDate").setParameter("fromDate", moment().startOf("year").toDate());
        }
        if (queryParams.toDate) {
            query.andWhere("m.createdAt <= :toDate").setParameter("toDate", moment(queryParams.toDate).endOf("d").toDate());
        } else {
            query.andWhere("m.createdAt <= :toDate").setParameter("toDate", moment().endOf("day").toDate());
        }
        if (queryParams.userId) {
            query.andWhere("m.userId = :userId").setParameter("userId", queryParams.userId);
        }
        const logs = await query.getMany();
        return logs;
    }

    async getById(id: number) {
        const connection = await dbUtil.getDefaultConnection();
        const log = await connection.getRepository(MetadataLog).findOne({ where: { id } });
        return log;
    }

    async create(model: any) {
        const validatedModel = await createValidator.validateAsync(model, { allowUnknown: true });
        const connection = await dbUtil.getDefaultConnection();
        const log = new MetadataLog();
        Object.assign(log, validatedModel);
        const newLog = await connection.manager.save(log);
        return newLog;
    }

    async update(model: any) {
        const validatedModel = await createValidator.validateAsync(model, { allowUnknown: true });
        const connection = await dbUtil.getDefaultConnection();
        let PolicyId = model.PolicyId;
        const query = connection.getRepository(MetadataLog).createQueryBuilder("m")
            .where("m.isDeleted = :isDeleted").setParameter("isDeleted", false)
            .orderBy("m.createdAt", "DESC");
        query.andWhere("m.PolicyId").setParameter("PolicyId", PolicyId);
        const asetLog = await query.getOneOrFail();
        let creatAt = asetLog.createdAt;
        Object.assign(asetLog, validatedModel);
        asetLog.createdAt = creatAt;
        const newLog = await connection.manager.save(asetLog);
        return newLog;
    }

    async transfer(model: any) {
        const validatedModel = await createValidator.validateAsync(model, { allowUnknown: true });
        const connection = await dbUtil.getDefaultConnection();
        const log = new MetadataLog();
        Object.assign(log, validatedModel);
        const newLog = await connection.manager.save(log);
        return newLog;
    }

    async createRandom() {

        const connection = await dbUtil.getDefaultConnection();
        const log = new MetadataLog();
        log.userId = await this.getRandomInt(1, 15000).toString();
        log.createdAt = new Date();
        const newLog = await connection.manager.save(log);
        return newLog;
    }

    async getRandomInt(min: number, max: number) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min) + min);
    }

    async publishMetadataLog(model: any, action: string) {

        const connection = await dbUtil.getDefaultConnection();
        const log = new MetadataLog();
        log.userId = model.userId;
        log.userName = model.userName;
        log.userToken = model.userToken;
        log.endpointUsed = model.endpointUsed;
        log.createdAt = model.createdAt;
        log.action = action;
        log.providerId = model.providerId;
        log.providerName = model.providerName;
        log.type = model.type;
        log.datasets = JSON.stringify(model.datasets);
        log.ipAddress = model.ipAddress;
        log.catalogId = model.catalogId;
        log.catalogName = model.catalogName;
        const newLog = await connection.manager.save(log);
        return newLog;
    }

    async getWithFilters(queryParams: any) {
        const connection = await dbUtil.getDefaultConnection();

        const query = connection.getRepository(MetadataLog).createQueryBuilder("m")
            .orderBy("m.createdAt", "DESC");

        if (queryParams.fromDate !== 'undefined' && queryParams.fromDate) {
            query.andWhere("m.createdAt >= :fromDate").setParameter("fromDate", moment(queryParams.fromDate).startOf("d").toDate());
        }
        if (queryParams.toDate !== 'undefined' && queryParams.toDate) {
            query.andWhere("m.createdAt <= :toDate").setParameter("toDate", moment(queryParams.toDate).endOf("d").toDate());
        }
        if (queryParams.catalogId) {
            query.andWhere("m.catalogId = :catalogId").setParameter("catalogId", queryParams.catalogId);
        } else if (queryParams.providerName) {
            query.andWhere("m.catalogName = :catalogName").setParameter("catalogName", queryParams.catalogName);
        }
        if (queryParams.ipAddress) {
            query.andWhere("m.ipAddress = :ipAddress").setParameter("ipAddress", queryParams.ipAddress);
        }
        const logs = await query.getMany();
        return logs;
    }

}

export default new MetadataLogService();