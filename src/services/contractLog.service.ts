/**
 * Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * SPDX-License-Identifier: MIT
 * 
 * Contributors:
 * Georgios Nikolaidis - Author
 * Vasileios Siopidis - Coauthor
 * Konstantinos Votis - Coauthor
 */

import Joi from '@hapi/joi';
import moment from "moment";
import dbUtil from '../utils/db.util';
import { ContractLog } from '../models/contractLog.model';
import { ContractOfferLog } from '../models/contractOfferLog.model';
import { ContractAgreementLog } from '../models/contractAgreementLog.model';
import { logger } from "../utils/logger.util";
import { contractsPerConsumerViewModel } from '../viewModels/contractsPerConsumerViewModel';
import { contractsPerConsumerLogViewModel } from '../viewModels/contractsPerConsumerLogViewModel';
import { ContractOffer } from '../models/contractOffer.model';
import { ContractAgreement } from '../models/contractAgreement.model';


const createValidator = Joi.object({
    companyName: Joi.string().required()
});

class ContractLogService {
    async getAll() {
        const connection = await dbUtil.getDefaultConnection();
        const logs = await connection.getRepository(ContractLog).find({
            where: {
                isDeleted: false
            }
        });
        return logs;
    }

    async getByDate(queryParams: any) {
        const connection = await dbUtil.getDefaultConnection();
        const query = connection.getRepository(ContractLog).createQueryBuilder("m")
            .where("m.isDeleted = :isDeleted").setParameter("isDeleted", false)
            .orderBy("m.createdAt", "DESC");

        if (queryParams.fromDate) {
            query.andWhere("m.createdAt >= :fromDate").setParameter("fromDate", moment(queryParams.fromDate).startOf("d").toDate());
        } else {
            query.andWhere("m.createdAt >= :fromDate").setParameter("fromDate", moment().startOf("year").toDate());
        }
        if (queryParams.toDate) {
            query.andWhere("m.createdAt <= :toDate").setParameter("toDate", moment(queryParams.toDate).endOf("d").toDate());
        } else {
            query.andWhere("m.createdAt <= :toDate").setParameter("toDate", moment().endOf("day").toDate());
        }
        if (queryParams.userId) {
            query.andWhere("m.userId = :userId").setParameter("userId", queryParams.userId);
        }
        const logs = await query.getMany();
        return logs;
    }

    async getById(id: number) {
        const connection = await dbUtil.getDefaultConnection();
        const log = await connection.getRepository(ContractLog).findOne({ where: { id } });
        return log;
    }

    async getAgreementsPerConsumer(queryParams: any) {
        const connection = await dbUtil.getDefaultConnection();
        let vm = new contractsPerConsumerViewModel()
        vm.agreements = 0;
        const query = connection.getRepository(ContractLog).createQueryBuilder("m")
            .where("m.isDeleted = :isDeleted").setParameter("isDeleted", false)

        if (queryParams.providerId !== 'undefined' && queryParams.providerId) {
            query.andWhere("m.providerId = :providerId").setParameter("providerId", queryParams.providerId);
        } else if (queryParams.providerName !== 'undefined' && queryParams.providerName) {
            query.andWhere("m.providerName = :providerName").setParameter("providerName", queryParams.providerName);
        }

        if (queryParams.fromDate) {
            query.andWhere("m.createdAt >= :fromDate").setParameter("fromDate", moment(queryParams.fromDate).startOf("d").toDate());
        }
        if (queryParams.toDate) {
            query.andWhere("m.createdAt <= :toDate").setParameter("toDate", moment(queryParams.toDate).endOf("d").toDate());
        }

        if (queryParams.consumerId !== 'undefined' && queryParams.consumerId) {
            query.andWhere("m.cοnsumerId = :consumerId").setParameter("consumerId", queryParams.consumerId);
        } else if (queryParams.cοnsumerName !== 'undefined' && queryParams.cοnsumerName) {
            query.andWhere("m.cοnsumerName = :cοnsumerName").setParameter("cοnsumerName", queryParams.cοnsumerName);
        }
        query.andWhere("m.isNegotiated = :isNegotiated").setParameter("isNegotiated", true);
        query.orderBy("m.createdAt", "DESC");
        const logs = await query.getMany();
        let agreementsPerConsumer = [] as contractsPerConsumerLogViewModel[];
        if (logs && logs.length > 0) {

            vm.agreements = logs.length;
            const uniqueArray = [] as String[];
            logs.forEach(item => {

                if (!uniqueArray.includes(item.cοnsumerId)) {
                    uniqueArray.push(item.cοnsumerId)
                    let m = new contractsPerConsumerLogViewModel();
                    m.consumerId = item.cοnsumerId;
                    m.agreements = 1;
                    m.assets.push(item.assetName);
                    m.time_range.first_agreement = item.validFromDate.toISOString();
                    m.time_range.latest_agreement = item.validToDate.toISOString();
                    agreementsPerConsumer.push(m);
                }
                else {
                    let m = agreementsPerConsumer.find(obj => obj.consumerId === item.cοnsumerId);
                    if (m) {
                        m.agreements++;
                        m.assets.push(item.assetName);
                        if (item.validFromDate.toISOString() < m.time_range.first_agreement) {
                            m.time_range.first_agreement = item.validFromDate.toISOString();
                        }
                        if (item.validToDate.toISOString() > m.time_range.latest_agreement) {
                            m.time_range.latest_agreement = item.validToDate.toISOString();
                        }
                    }
                }
            });
            vm.agreements_per_consumer = agreementsPerConsumer;
            return vm;
        }
        return vm;
    }

    async getNegotiatedContracts(queryParams: any) {
        const connection = await dbUtil.getDefaultConnection();

        const query = connection.getRepository(ContractLog).createQueryBuilder("m")
            .where("m.isDeleted = :isDeleted").setParameter("isDeleted", false);

        if (queryParams.providerId !== 'undefined' && queryParams.providerId) {
            query.andWhere("m.providerId = :providerId").setParameter("providerId", queryParams.providerId);
        } else if (queryParams.providerName !== 'undefined' && queryParams.providerName) {
            query.andWhere("m.providerName = :providerName").setParameter("providerName", queryParams.providerName);
        }
        if (queryParams.fromDate) {
            query.andWhere("m.createdAt >= :fromDate").setParameter("fromDate", moment(queryParams.fromDate).startOf("d").toDate());
        }
        if (queryParams.toDate) {
            query.andWhere("m.createdAt <= :toDate").setParameter("toDate", moment(queryParams.toDate).endOf("d").toDate());
        }

        if (queryParams.cοnsumerId !== 'undefined' && queryParams.cοnsumerId) {
            query.andWhere("m.cοnsumerId = :cοnsumerId").setParameter("cοnsumerId", queryParams.cοnsumerId);
        } else if (queryParams.cοnsumerName !== 'undefined' && queryParams.cοnsumerName) {
            query.andWhere("m.cοnsumerName = :cοnsumerName").setParameter("cοnsumerName", queryParams.cοnsumerName);
        }

        query.andWhere("m.isNegotiated = :isNegotiated").setParameter("isNegotiated", true);
        query.orderBy("m.createdAt", "DESC");

        const logs = await query.getMany();
        let negotiatedContracts = [] as ContractLog[];
        if (logs && logs.length > 0) {
            for (let i = 0; i < logs.length; i++) {
                let result = negotiatedContracts.find(obj => obj.contractId === logs[i].contractId);
                if (result) {
                    continue;
                }
                else {
                    negotiatedContracts.push(logs[i]);
                }
            }
        }
        return negotiatedContracts;
    }

    async getContractOffersWithFilters(queryParams: any) {
        const connection = await dbUtil.getDefaultConnection();

        const query = connection.getRepository(ContractOfferLog).createQueryBuilder("m")
            .orderBy("m.createdAt", "DESC");

        if (queryParams.fromDate !== 'undefined' && queryParams.fromDate) {
            query.andWhere("m.createdAt >= :fromDate").setParameter("fromDate", moment(queryParams.fromDate).startOf("d").toDate());
        }
        if (queryParams.toDate !== 'undefined' && queryParams.toDate) {
            query.andWhere("m.createdAt <= :toDate").setParameter("toDate", moment(queryParams.toDate).endOf("d").toDate());
        }
        if (queryParams.providerId !== 'undefined' && queryParams.providerId) {
            query.andWhere("m.providerId = :providerId").setParameter("providerId", queryParams.providerId);
        } else if (queryParams.providerName !== 'undefined' && queryParams.providerName) {
            logger.info(query.getParameters());
            query.andWhere("m.providerName = :providerName").setParameter("providerName", queryParams.providerName);
        }
        if (queryParams.assetId !== 'undefined' && queryParams.assetId) {
            query.andWhere("m.assetId = :assetId").setParameter("assetId", queryParams.assetId);
        } else if (queryParams.assetName !== 'undefined' && queryParams.assetName) {
            logger.info(query.getParameters());
            query.andWhere("m.assetName = :assetName").setParameter("assetName", queryParams.assetName);
        }
        if (queryParams.action !== 'undefined' && queryParams.action) {
            query.andWhere("m.action = :action").setParameter("action", queryParams.action);
        }
        if (queryParams.ipAddress !== undefined && queryParams.ipAddress) {
            console.log(query.getParameters());
            query.andWhere("m.ipAddress = :ipAddress").setParameter("ipAddress", queryParams.ipAddress);
        }
        const logs = await query.getMany();
        return logs;
    }

    async getContractAgreementsWithFilters(queryParams: any) {
        const connection = await dbUtil.getDefaultConnection();

        const query = connection.getRepository(ContractAgreementLog).createQueryBuilder("m")
            .orderBy("m.createdAt", "DESC");

        if (queryParams.fromDate !== 'undefined' && queryParams.fromDate) {
            query.andWhere("m.createdAt >= :fromDate").setParameter("fromDate", moment(queryParams.fromDate).startOf("d").toDate());
        }
        if (queryParams.toDate !== 'undefined' && queryParams.toDate) {
            query.andWhere("m.createdAt <= :toDate").setParameter("toDate", moment(queryParams.toDate).endOf("d").toDate());
        }
        if (queryParams.validFromDate !== 'undefined' && queryParams.validFromDate) {
            query.andWhere("m.validFromDate >= :validFromDate").setParameter("validFromDate", moment(queryParams.validFromDate).startOf("d").toDate());
        } else if (queryParams.validFromDate !== 'undefined' && queryParams.validFromDate) {
            query.andWhere("m.validFromDate >= :validFromDate").setParameter("validFromDate", moment().startOf("year").toDate());
        }
        if (queryParams.validToDate !== undefined && queryParams.validToDate) {
            query.andWhere("m.validToDate <= :validToDate").setParameter("validToDate", moment(queryParams.validToDate).endOf("d").toDate());
        } else if (queryParams.validToDate !== 'undefined' && queryParams.validToDate) {
            logger.info(query.getParameters());
            query.andWhere("m.validToDate <= :validToDate").setParameter("validToDate", moment().endOf("day").toDate());
        }
        if (queryParams.providerId !== 'undefined' && queryParams.providerId) {
            query.andWhere("m.providerId = :providerId").setParameter("providerId", queryParams.providerId);
        } else if (queryParams.providerName !== 'undefined' && queryParams.providerName) {
            logger.info(query.getParameters());
            query.andWhere("m.providerName = :providerName").setParameter("providerName", queryParams.providerName);
        }
        if (queryParams.cοnsumerId !== 'undefined' && queryParams.cοnsumerId) {
            query.andWhere("m.cοnsumerId = :cοnsumerId").setParameter("cοnsumerId", queryParams.cοnsumerId);
        } else if (queryParams.cοnsumerName !== 'undefined' && queryParams.cοnsumerName) {
            logger.info(query.getParameters());
            query.andWhere("m.cοnsumerName = :cοnsumerName").setParameter("cοnsumerName", queryParams.cοnsumerName);
        }
        if (queryParams.assetId !== 'undefined' && queryParams.assetId) {
            query.andWhere("m.assetId = :assetId").setParameter("assetId", queryParams.assetId);
        } else if (queryParams.assetName !== 'undefined' && queryParams.assetName) {
            logger.info(query.getParameters());
            query.andWhere("m.assetName = :assetName").setParameter("assetName", queryParams.assetName);
        }
        if (queryParams.action !== 'undefined' && queryParams.action) {
            query.andWhere("m.action = :action").setParameter("action", queryParams.action);
        }
        if (queryParams.ipAddress !== undefined && queryParams.ipAddress) {
            console.log(query.getParameters());
            query.andWhere("m.ipAddress = :ipAddress").setParameter("ipAddress", queryParams.ipAddress);
        }
        const logs = await query.getMany();
        return logs;
    }

    async getWithFilters(queryParams: any) {
        const connection = await dbUtil.getDefaultConnection();

        const query = connection.getRepository(ContractLog).createQueryBuilder("m")
            .orderBy("m.createdAt", "DESC");

        if (queryParams.fromDate !== 'undefined' && queryParams.fromDate) {
            query.andWhere("m.createdAt >= :fromDate").setParameter("fromDate", moment(queryParams.fromDate).startOf("d").toDate());
        }
        if (queryParams.toDate !== 'undefined' && queryParams.toDate) {
            query.andWhere("m.createdAt <= :toDate").setParameter("toDate", moment(queryParams.toDate).endOf("d").toDate());
        }
        if (queryParams.validFromDate !== 'undefined' && queryParams.validFromDate) {
            query.andWhere("m.validFromDate >= :validFromDate").setParameter("validFromDate", moment(queryParams.validFromDate).startOf("d").toDate());
        } else if (queryParams.validFromDate !== 'undefined' && queryParams.validFromDate) {
            query.andWhere("m.validFromDate >= :validFromDate").setParameter("validFromDate", moment().startOf("year").toDate());
        }
        if (queryParams.validToDate !== undefined && queryParams.validToDate) {
            query.andWhere("m.validToDate <= :validToDate").setParameter("validToDate", moment(queryParams.validToDate).endOf("d").toDate());
        } else if (queryParams.validToDate !== 'undefined' && queryParams.validToDate) {
            logger.info(query.getParameters());
            query.andWhere("m.validToDate <= :validToDate").setParameter("validToDate", moment().endOf("day").toDate());
        }
        if (queryParams.providerId !== 'undefined' && queryParams.providerId) {
            query.andWhere("m.providerId = :providerId").setParameter("providerId", queryParams.providerId);
        } else if (queryParams.providerName !== 'undefined' && queryParams.providerName) {
            logger.info(query.getParameters());
            query.andWhere("m.providerName = :providerName").setParameter("providerName", queryParams.providerName);
        }
        if (queryParams.cοnsumerId !== 'undefined' && queryParams.cοnsumerId) {
            query.andWhere("m.cοnsumerId = :cοnsumerId").setParameter("cοnsumerId", queryParams.cοnsumerId);
        } else if (queryParams.cοnsumerName !== 'undefined' && queryParams.cοnsumerName) {
            logger.info(query.getParameters());
            query.andWhere("m.cοnsumerName = :cοnsumerName").setParameter("cοnsumerName", queryParams.cοnsumerName);
        }
        if (queryParams.assetId !== 'undefined' && queryParams.assetId) {
            query.andWhere("m.assetId = :assetId").setParameter("assetId", queryParams.assetId);
        } else if (queryParams.assetName !== 'undefined' && queryParams.assetName) {
            logger.info(query.getParameters());
            query.andWhere("m.assetName = :assetName").setParameter("assetName", queryParams.assetName);
        }
        if (queryParams.action !== 'undefined' && queryParams.action) {
            query.andWhere("m.action = :action").setParameter("action", queryParams.action);
        }
        if (queryParams.ipAddress !== undefined && queryParams.ipAddress) {
            console.log(query.getParameters());
            query.andWhere("m.ipAddress = :ipAddress").setParameter("ipAddress", queryParams.ipAddress);
        }
        const logs = await query.getMany();
        return logs;
    }


    async create(model: any) {
        const validatedModel = await createValidator.validateAsync(model, { allowUnknown: true });
        const connection = await dbUtil.getDefaultConnection();
        const log = new ContractLog();
        Object.assign(log, validatedModel);
        const newLog = await connection.manager.save(log);
        return newLog;
    }

    async update(model: any) {
        const validatedModel = await createValidator.validateAsync(model, { allowUnknown: true });
        const connection = await dbUtil.getDefaultConnection();
        let ContractId = model.ContractId;
        const query = connection.getRepository(ContractLog).createQueryBuilder("m")
            .where("m.isDeleted = :isDeleted").setParameter("isDeleted", false)
            .orderBy("m.createdAt", "DESC");
        query.andWhere("m.ContractId").setParameter("ContractId", ContractId);
        const asetLog = await query.getOneOrFail();
        let creatAt = asetLog.createdAt;
        Object.assign(asetLog, validatedModel);
        asetLog.createdAt = creatAt;
        const newLog = await connection.manager.save(asetLog);
        return newLog;
    }

    async transfer(model: any) {
        const validatedModel = await createValidator.validateAsync(model, { allowUnknown: true });
        const connection = await dbUtil.getDefaultConnection();
        const log = new ContractLog();
        Object.assign(log, validatedModel);
        const newLog = await connection.manager.save(log);
        return newLog;
    }

    async createRandom() {
        const connection = await dbUtil.getDefaultConnection();
        const log = new ContractLog();
        log.userId = await this.getRandomInt(1, 15000).toString();
        log.createdAt = new Date();
        const newLog = await connection.manager.save(log);
        return newLog;
    }

    async getRandomInt(min: number, max: number) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min) + min);
    }

    async createContractAgreementLog(model: any, action: string) {

        let res = await this.createOrUpdateContractAgreement(model, action);
        const connection = await dbUtil.getDefaultConnection();
        const log = new ContractAgreementLog();
        log.userId = model.userId;
        log.userName = model.userName;
        log.action = action;
        log.assetId = model.assetId;
        log.assetName = model.assetName;
        log.policyId = model.assetPolicyId;
        if (action === 'negotiate') {
            log.isNegotiated = true;
        }
        if (action === 'reject') {
            log.isRejected = true;
        }
        log.isDeleted = model.isDeleted;
        if (action === 'delete') {
            log.isDeleted = true;
        }
        log.contractAgreementId = model.contractAgreementId;
        log.contractOfferId = model.contractOfferId;
        log.cοnsumerId = model.consumerId;
        log.cοnsumerName = model.consumerName;
        log.endpointUsed = model.endpointUsed;
        log.providerId = model.providerId;
        log.providerName = model.providerName;
        log.signingDate = model.signingDate;
        log.createdAt = model.createdAt;
        log.ipAddress = model.ipAddress;
        log.validFromDate = model.validFromDate;
        log.validToDate = model.validToDate;
        const newLog = await connection.manager.save(log);
        return newLog;
    }

    async createContractOfferLog(model: any, action: string) {

        let res = await this.createOrUpdateContractOffer(model, action);
        const connection = await dbUtil.getDefaultConnection();
        const log = new ContractOfferLog();
        log.userId = model.userId;
        log.userName = model.userName;
        log.action = action;
        //log.assets = model.assets;
        log.assetPolicyId = model.assetPolicyId;
        log.contractPolicyId = model.contractPolicyId;
        log.isDeleted = model.isDeleted;
        if (action === 'delete') {
            log.isDeleted = true;
        }
        log.connectorId = model.connectorId;
        log.contractOfferId = model.contractOfferId;
        log.endpointUsed = model.endpointUsed;
        log.providerId = model.providerId;
        log.createdAt = model.createdAt;
        log.ipAddress = model.ipAddress;

        const newLog = await connection.manager.save(log);
        return newLog;
    }

    async createOrUpdateContractOffer(model: any, action: string) {

        const connection = await dbUtil.getDefaultConnection();
        let contractOfferId = model.contractOfferId;
        const query = connection.getRepository(ContractOffer).createQueryBuilder("m")
            .orderBy("m.createdAt", "DESC");
        query.andWhere("m.contractOfferId").setParameter("contractOfferId", contractOfferId);
        let log = await query.getOne();
        let obj;
        if (!log) {
            obj = new ContractOffer();
            obj.userId = model.userId;
            obj.userName = model.userName;
            //log.assets = model.assets;
            obj.assetPolicyId = model.assetPolicyId;
            obj.contractPolicyId = model.contractPolicyId;
            obj.isDeleted = model.isDeleted;
            if (action === 'delete') {
                obj.isDeleted = true;
            }
            obj.connectorId = model.connectorId;
            obj.contractOfferId = model.contractOfferId;
            obj.providerId = model.providerId;
            obj.createdAt = model.createdAt;
        }
        else {
            obj = log;
            obj.userId = model.userId;
            obj.userName = model.userName;
            //log.assets = model.assets;
            obj.assetPolicyId = model.assetPolicyId;
            obj.contractPolicyId = model.contractPolicyId;
            obj.isDeleted = model.isDeleted;
            if (action === 'delete') {
                obj.isDeleted = true;
            }
            obj.connectorId = model.connectorId;
            obj.contractOfferId = model.contractOfferId;
            obj.providerId = model.providerId;
            obj.createdAt = model.createdAt;
        }
        let newObj = await connection.manager.save(obj);
        return newObj;

    }

    async createOrUpdateContractAgreement(model: any, action: string) {

        const connection = await dbUtil.getDefaultConnection();
        let contractAgreementId = model.contractAgreementId;
        const query = connection.getRepository(ContractAgreement).createQueryBuilder("m")
            .orderBy("m.createdAt", "DESC");
        query.andWhere("m.contractAgreementId").setParameter("contractAgreementId", contractAgreementId);
        let log = await query.getOne();
        let obj;
        if (!log) {
            obj = new ContractAgreement();
            obj.userId = model.userId;
            obj.userName = model.userName;
            obj.action = action;
            obj.assetId = model.assetId;
            obj.assetName = model.assetName;
            obj.policyId = model.assetPolicyId;
            if (action === 'negotiate') {
                obj.isNegotiated = true;
            }
            if (action === 'reject') {
                obj.isRejected = true;
            }
            obj.isDeleted = model?.isDeleted ?? false;
            if (action === 'delete') {
                obj.isDeleted = true;
            }
            obj.contractAgreementId = model.contractAgreementId;
            obj.contractOfferId = model.contractOfferId;
            obj.cοnsumerId = model.consumerId;
            obj.cοnsumerName = model.consumerName;
            obj.providerId = model.providerId;
            obj.providerName = model.providerName;
            obj.signingDate = model.signingDate;
            obj.createdAt = model.createdAt;
            obj.validFromDate = model.validFromDate;
            obj.validToDate = model.validToDate;
        }
        else {
            obj = log;
            obj.userId = model.userId;
            obj.userName = model.userName;
            obj.action = action;
            obj.assetId = model.assetId;
            obj.assetName = model.assetName;
            obj.policyId = model.assetPolicyId;
            if (action === 'negotiate') {
                obj.isNegotiated = true;
            }
            if (action === 'reject') {
                obj.isRejected = true;
            }
            obj.isDeleted = model.isDeleted;
            if (action === 'delete') {
                obj.isDeleted = true;
            }
            obj.contractAgreementId = model.contractAgreementId;
            obj.contractOfferId = model.contractOfferId;
            obj.cοnsumerId = model.consumerId;
            obj.cοnsumerName = model.consumerName;
            obj.providerId = model.providerId;
            obj.providerName = model.providerName;
            obj.signingDate = model.signingDate;
            obj.createdAt = model.createdAt;
            obj.validFromDate = model.validFromDate;
            obj.validToDate = model.validToDate;
        }
        let newObj = await connection.manager.save(obj);
        return newObj;

    }

    async createContractLog(model: any, action: string) {

        const connection = await dbUtil.getDefaultConnection();
        const log = new ContractLog();
        log.userId = model.userId;
        log.userName = model.userName;
        log.action = action;
        log.status = model.status;
        log.assetId = model.assetId;
        log.assetName = model.assetName;
        log.assetPolicyId = model.assetPolicyId;
        log.contractPolicyId = model.contractPolicyId;
        if (action === 'negotiate') {
            log.isNegotiated = true;
        }
        if (action === 'reject') {
            log.isRejected = true;
        }
        log.isDeleted = model.isDeleted;
        if (action === 'delete') {
            log.isDeleted = true;
        }
        log.type = model.type;
        log.connectorId = model.connectorId;
        log.contractId = model.contractId;
        log.cοnsumerId = model.consumerId;
        log.endpointUsed = model.endpointUsed;
        log.providerId = model.providerId;
        log.createdAt = model.createdAt;
        log.ipAddress = model.ipAddress;
        log.validFromDate = model.validFromDate;
        log.validToDate = model.validToDate;
        const newLog = await connection.manager.save(log);
        return newLog;
    }


}

export default new ContractLogService();