/**
 * Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * SPDX-License-Identifier: MIT
 * 
 * Contributors:
 * Georgios Nikolaidis - Author
 * Vasileios Siopidis - Coauthor
 * Konstantinos Votis - Coauthor
 */

import Joi from '@hapi/joi';
import moment from "moment";
import dbUtil from '../utils/db.util';
import { AssetLog } from '../models/assetLog.model';
import sha256 from 'crypto-js/sha256';
import { logger } from "../utils/logger.util";
import { HASH_SECRET } from '../utils/config.util';
import { NODE_ENV } from '../utils/config.util';
import { AppConstants } from '../utils/constants';
import contractLogService from '../services/contractLog.service';
import { incomingAssetRequestViewModel } from '../viewModels/incomingAssetRequestViewModel';
import { incomingAssetLogRequestViewModel } from '../viewModels/incomingAssetLogRequestViewModel';
import { popularAssetViewModel } from '../viewModels/popularAssetViewModel';
import { unUsedAssetsViewModel } from '../viewModels/unUsedAssetsViewModel';
import { unUsedConsumerAssetViewModel } from '../viewModels/unUsedAssetsViewModel';

const createValidator = Joi.object({
    providerId: Joi.string().required()
});

class AssetLogService {
    async getAll() {
        const connection = await dbUtil.getDefaultConnection();
        const logs = await connection.getRepository(AssetLog).find({
            where: {
                isDeleted: false
            }
        });

        return logs;
    }

    async getByDate(queryParams: any) {
        const connection = await dbUtil.getDefaultConnection();
        const query = connection.getRepository(AssetLog).createQueryBuilder("m")
            .where("m.isDeleted = :isDeleted").setParameter("isDeleted", false)
            .orderBy("m.createdAt", "DESC");

        if (queryParams.fromDate) {
            query.andWhere("m.createdAt >= :fromDate").setParameter("fromDate", moment(queryParams.fromDate).startOf("d").toDate());
        } else {
            query.andWhere("m.createdAt >= :fromDate").setParameter("fromDate", moment().startOf("year").toDate());
        }
        if (queryParams.toDate) {
            query.andWhere("m.createdAt <= :toDate").setParameter("toDate", moment(queryParams.toDate).endOf("d").toDate());
        } else {
            query.andWhere("m.createdAt <= :toDate").setParameter("toDate", moment().endOf("day").toDate());
        }
        if (queryParams.userId) {
            query.andWhere("m.userId = :userId").setParameter("userId", queryParams.userId);
        }

        const logs = await query.getMany();
        return logs;
    }

    async getUnusedAssets(queryParams: any) {


        let unusedConsumerAssets = [] as unUsedConsumerAssetViewModel[];
        let unusedAssets = new unUsedAssetsViewModel(unusedConsumerAssets);
        let negotiatedContracts = await contractLogService.getNegotiatedContracts(queryParams);
        // let contractIds = negotiatedContracts.map(a => a.contractId);
        if (negotiatedContracts && negotiatedContracts.length > 0) {

            let assetIds = negotiatedContracts.map(a => a.assetId);
            let page = 1;
            let pageSize = 20;

            if (queryParams.page && queryParams.page > 0) {
                page = queryParams.page;
            }
            if (queryParams.pageSize && queryParams.pageSize > 0) {
                pageSize = queryParams.pageSize;
            }
            let skipSize = (page - 1) * pageSize;
            const connection = await dbUtil.getDefaultConnection();
            const query = connection.getRepository(AssetLog).createQueryBuilder("m")
                .where("m.isDeleted = :isDeleted").setParameter("isDeleted", false)

            if (queryParams.providerId !== 'undefined' && queryParams.providerId) {
                query.andWhere("m.providerId = :providerId").setParameter("providerId", queryParams.providerId);
            } else if (queryParams.providerName !== 'undefined' && queryParams.providerName) {
                query.andWhere("m.providerName = :providerName").setParameter("providerName", queryParams.providerName);
            }
            query.andWhere("m.action != :action").setParameter("action", "transfer");
            query.andWhere("m.assetId IN (:assetIds)").setParameter("assetIds", assetIds);
            query.orderBy("m.createdAt", "DESC");
            query.groupBy("m.assetId");
            query.skip(skipSize);
            query.take(pageSize);
            const logs = await query.getMany();

            if (logs && logs.length > 0) {
                const uniqueArray = [] as String[];
                logs.forEach(item => {
                    let contract = negotiatedContracts.find(x => x.assetId == item.assetId)
                    if (contract && !uniqueArray.includes(item.assetId)) {
                        uniqueArray.push(item.assetId)
                        let vm = new unUsedConsumerAssetViewModel();
                        vm.assetId = item.assetId;
                        vm.assetName = item.assetName;
                        vm.creation_date = contract.createdAt.toISOString();
                        unusedAssets.untransferred_assets.push(vm);
                        unusedAssets.untransferred_assets_count++;
                    }
                });
                return unusedAssets;
            }
            return logs;

        }
        else {
            return [];
        }

    }

    async getPopularAssets(queryParams: any) {


        let vm = new Array() as popularAssetViewModel[];
        let page = 1;
        let pageSize = 20;

        if (queryParams.page && queryParams.page > 0) {
            page = queryParams.page;
        }
        if (queryParams.pageSize && queryParams.pageSize > 0) {
            pageSize = queryParams.pageSize;
        }
        let skipSize = (page - 1) * pageSize;
        const connection = await dbUtil.getDefaultConnection();
        const query = connection.getRepository(AssetLog).createQueryBuilder("m")
            .where("m.isDeleted = :isDeleted").setParameter("isDeleted", false);


        if (queryParams.providerId !== 'undefined' && queryParams.providerId) {
            query.andWhere("m.providerId = :providerId").setParameter("providerId", queryParams.providerId);
        } else if (queryParams.providerName !== 'undefined' && queryParams.providerName) {
            query.andWhere("m.providerName = :providerName").setParameter("providerName", queryParams.providerName);
        }
        if (queryParams.fromDate) {
            query.andWhere("m.createdAt >= :fromDate").setParameter("fromDate", moment(queryParams.fromDate).startOf("d").toDate());
        }
        if (queryParams.toDate) {
            query.andWhere("m.createdAt <= :toDate").setParameter("toDate", moment(queryParams.toDate).endOf("d").toDate());
        }

        query.andWhere("m.action = :action").setParameter("action", "transfer");
        query.orderBy("m.createdAt", "DESC");
        query.skip(skipSize);
        query.take(pageSize);

        const logs = await query.getMany();


        if (logs && logs.length > 0) {
            const uniqueArray = [] as String[];
            logs.forEach(item => {


                if (!uniqueArray.includes(item.assetId)) {
                    uniqueArray.push(item.assetId)
                    let m = new popularAssetViewModel();
                    m.assetId = item.assetId;
                    m.assetName = item.assetName;
                    m.transferredTimes = 1
                    m.startedFrom = item.createdAt.toISOString();
                    m.endedTo = item.createdAt.toISOString();
                    m.consumers.push(item.cοnsumerName);
                    vm.push(m);
                }
                else {
                    let m = vm.find(obj => obj.assetId === item.assetId);
                    if (m) {
                        m.transferredTimes++;
                        let c = m.consumers.find(o => o === item.cοnsumerName);
                        if (!c) {
                            m.consumers.push(item.cοnsumerName);
                        }

                        if (item.createdAt.toISOString() < m.startedFrom) {
                            m.startedFrom = item.createdAt.toISOString();
                        }
                        if (item.createdAt.toISOString() > m.endedTo) {
                            m.endedTo = item.createdAt.toISOString();
                        }
                    }
                }
            });

            return vm;
        }
        return vm;
    }

    async getIncomingRequests(queryParams: any) {


        let vm = new incomingAssetRequestViewModel();

        const connection = await dbUtil.getDefaultConnection();
        const query = connection.getRepository(AssetLog).createQueryBuilder("m")
            .where("m.isDeleted = :isDeleted").setParameter("isDeleted", false)
            .orderBy("m.createdAt", "DESC");

        if (queryParams.providerId !== 'undefined' && queryParams.providerId) {
            query.andWhere("m.providerId = :providerId").setParameter("providerId", queryParams.providerId);
        } else if (queryParams.providerName !== 'undefined' && queryParams.providerName) {
            query.andWhere("m.providerName = :providerName").setParameter("providerName", queryParams.providerName);
        }
        query.andWhere("m.action = :action").setParameter("action", "transfer");
        query.orderBy("m.createdAt", "DESC");

        const logs = await query.getMany();

        let requestPerAsset = [] as incomingAssetLogRequestViewModel[];
        if (logs && logs.length > 0) {

            vm.incoming_requests = logs.length;
            vm.accepted_requests = logs.length;
            vm.rejected_requests = 0;


            const uniqueArray = [] as String[];
            logs.forEach(item => {
                if (!uniqueArray.includes(item.assetId)) {
                    uniqueArray.push(item.assetId)
                    let m = new incomingAssetLogRequestViewModel();
                    m.asset_id = item.assetId;
                    m.asset_name = item.assetName;
                    m.incoming_requests = 1
                    m.accepted_requests = 1;
                    m.rejected_requests = 0;
                    requestPerAsset.push(m);
                }
                else {
                    let m = requestPerAsset.find(obj => obj.asset_id === item.assetId);
                    if (m) {
                        m.incoming_requests++;
                        m.accepted_requests++;
                    }
                }
            });
            vm.requests_per_asset = requestPerAsset;
            return vm;
        }
        return vm;
    }

    async getWithFilters(queryParams: any) {
        const connection = await dbUtil.getDefaultConnection();

        const query = connection.getRepository(AssetLog).createQueryBuilder("m")
            .orderBy("m.createdAt", "DESC");

        if (queryParams.fromDate !== 'undefined' && queryParams.fromDate) {
            query.andWhere("m.createdAt >= :fromDate").setParameter("fromDate", moment(queryParams.fromDate).startOf("d").toDate());
        }

        if (queryParams.toDate !== 'undefined' && queryParams.toDate) {
            query.andWhere("m.createdAt <= :toDate").setParameter("toDate", moment(queryParams.toDate).endOf("d").toDate());
        }
        if (queryParams.providerId) {
            query.andWhere("m.providerId = :providerId").setParameter("providerId", queryParams.providerId);
        } else if (queryParams.providerName) {
            query.andWhere("m.providerName = :providerName").setParameter("providerName", queryParams.providerName);
        }

        if (queryParams.cοnsumerId !== 'undefined' && queryParams.cοnsumerId) {
            query.andWhere("m.cοnsumerId = :cοnsumerId").setParameter("cοnsumerId", queryParams.cοnsumerId);
        } else if (queryParams.cοnsumerName !== 'undefined' && queryParams.cοnsumerName) {
            query.andWhere("m.cοnsumerName = :cοnsumerName").setParameter("cοnsumerName", queryParams.cοnsumerName);
        }

        if (queryParams.assetId !== 'undefined' && queryParams.assetId) {
            query.andWhere("m.assetId = :assetId").setParameter("assetId", queryParams.assetId);
        } else if (queryParams.assetName !== 'undefined' && queryParams.assetName) {
            query.andWhere("m.assetName = :assetName").setParameter("assetName", queryParams.assetName);
        }

        if (queryParams.action !== 'undefined' && queryParams.action) {
            query.andWhere("m.action = :action").setParameter("action", queryParams.action);
        }

        if (queryParams.ipAddress !== undefined && queryParams.ipAddress) {
            console.log(query.getParameters());
            query.andWhere("m.ipAddress = :ipAddress").setParameter("ipAddress", queryParams.ipAddress);
        }


        const logs = await query.getMany();
        return logs;
    }

    async providerFilterQuery(queryParams: any) {
        const connection = await dbUtil.getDefaultConnection();

        const query = connection.getRepository(AssetLog).createQueryBuilder("m")
            .orderBy("m.createdAt", "DESC");

        if (queryParams.providerId) {
            query.andWhere("m.providerId = :providerId").setParameter("providerId", queryParams.providerId);
        } else if (queryParams.providerName) {
            query.andWhere("m.providerName = :providerName").setParameter("providerName", queryParams.providerName);
        }
        return query;
    }


    async assetFilterQuery(queryParams: any) {
        const connection = await dbUtil.getDefaultConnection();

        const query = connection.getRepository(AssetLog).createQueryBuilder("m")
            .orderBy("m.createdAt", "DESC");

        if (queryParams.assetId) {
            query.andWhere("m.assetId = :assetId").setParameter("assetId", queryParams.assetId);
        } else if (queryParams.assetName) {
            query.andWhere("m.assetName = :assetName").setParameter("assetName", queryParams.assetName);
        }
        return query;
    }

    async consumerFilterQuery(queryParams: any) {
        const connection = await dbUtil.getDefaultConnection();

        const query = connection.getRepository(AssetLog).createQueryBuilder("m")
            .orderBy("m.createdAt", "DESC");

        if (queryParams.assetId) {
            query.andWhere("m.assetId = :assetId").setParameter("assetId", queryParams.assetId);
        } else if (queryParams.assetName) {
            query.andWhere("m.assetName = :assetName").setParameter("assetName", queryParams.assetName);
        }
        return query;
    }

    async getById(id: number) {
        const connection = await dbUtil.getDefaultConnection();
        const log = await connection.getRepository(AssetLog).findOne({ where: { id } });

        return log;
    }

    async getByAssetId(queryParams: any) {
        const connection = await dbUtil.getDefaultConnection();
        const query = connection.getRepository(AssetLog).createQueryBuilder("m")
            .orderBy("m.createdAt", "DESC");

        if (queryParams.assetId) {
            query.andWhere("m.assetId = :assetId").setParameter("assetId", queryParams.assetId);
        }


        const logs = await query.getMany();
        return logs;
    }

    /**
 * Asynchronously creates a new asset log entry.
 * 
 * @param {any} model - The model containing the asset log information to be created.
 * @returns {Promise<AssetLog>} A promise that resolves to the newly created asset log entry.
 */

    async create(model: any) {
        const validatedModel = await createValidator.validateAsync(model, { allowUnknown: true });
        const connection = await dbUtil.getDefaultConnection();
        const log = new AssetLog();
        Object.assign(log, validatedModel);
        //log.createdAt = new Date().getMilliseconds();

        const newLog = await connection.manager.save(log);
        return newLog;
    }

    /**
 * Asynchronously updates an existing asset log entry.
 * 
 * @param {any} model - The model containing the updated asset log information.
 * @returns {Promise<AssetLog>} A promise that resolves to the updated asset log entry.
 */
    async update(model: any) {
        const validatedModel = await createValidator.validateAsync(model, { allowUnknown: true });
        const connection = await dbUtil.getDefaultConnection();
        let assetId = model.assetId;
        const query = connection.getRepository(AssetLog).createQueryBuilder("m")
            .where("m.isDeleted = :isDeleted").setParameter("isDeleted", false)
            .orderBy("m.createdAt", "DESC");
        query.andWhere("m.assetId").setParameter("assetId", assetId);
        const asetLog = await query.getOneOrFail();
        let creatAt = asetLog.createdAt;
        Object.assign(asetLog, validatedModel);
        asetLog.createdAt = creatAt;


        const newLog = await connection.manager.save(asetLog);
        return newLog;
    }

    async transfer(model: any) {
        const validatedModel = await createValidator.validateAsync(model, { allowUnknown: true });
        const connection = await dbUtil.getDefaultConnection();
        const log = new AssetLog();
        Object.assign(log, validatedModel);
        //log.createdAt = new Date().getMilliseconds();

        const newLog = await connection.manager.save(log);
        return newLog;
    }

    async transferWithHashedData(model: any) {
        const validatedModel = await createValidator.validateAsync(model, { allowUnknown: true });

        let postedHash = model.requestHash;
        const connection = await dbUtil.getDefaultConnection();
        const log = new AssetLog();
        Object.assign(log, validatedModel);
        //log.createdAt = new Date().getMilliseconds();

        const newLog = await connection.manager.save(log);
        return newLog;
    }

    async createRandom() {

        const connection = await dbUtil.getDefaultConnection();
        const log = new AssetLog();
        log.userId = await this.getRandomInt(1, 15000).toString();
        log.assetId = '350ba92e-7658-4816-b6f6-579529d1cc7f';
        log.createdAt = new Date();

        const newLog = await connection.manager.save(log);
        return newLog;
    }

    async getRandomInt(min: number, max: number) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min) + min);
    }

    async createAssetLog(model: any, action: string) {

        if (action === 'transferWithHashedData') {
            let postedHash = model.requestHash;
            const nonce = HASH_SECRET;
            logger.info('postedHash : ' + postedHash);
            logger.info('nonce : ' + nonce);
            logger.info('NODE_ENV : ' + NODE_ENV);
            logger.info('AppConstants.HASH_SECRET : ' + AppConstants.HASH_SECRET);

            if (model.requestHash) {
                delete model.requestHash;
            }
            let hashDigest = sha256(nonce + JSON.stringify(model));
            logger.info('hashDigest : ' + hashDigest.toString());
            if (postedHash !== hashDigest.toString()) {
                return "errorhash";
            }
            action = 'transfer';
        }

        const connection = await dbUtil.getDefaultConnection();
        const log = new AssetLog();
        log.userId = model.userId;
        log.action = action;
        log.assetId = model.assetId;
        log.aggreementId = model.aggreementId;
        log.cοnsumerId = model.consumerId;
        log.policyId = model.policyId;
        log.isDeleted = model.isDeleted;
        if (action === 'delete') {
            log.isDeleted = true;
        }
        log.endpointUsed = model.endpointUsed;
        log.providerName = model.providerName;
        log.providerId = model.providerId;
        log.createdAt = model.createdAt;
        log.assetName = model.assetName;
        log.contentType = model.contentType;
        log.version = model.version;
        log.container = model.container;
        log.blobName = model.blobName;
        log.accountName = model.accountName;
        log.providerName = model.providerName;
        log.userName = model.userName;
        log.transferId = model.transferId;
        log.cοnsumerId = model.cοnsumerId;
        log.cοnsumerName = model.cοnsumerName;
        log.accessPolicyId = model.accessPolicyId;
        log.contractId = model.contractId;
        log.ipAddress = model.ipAddress;
        const newLog = await connection.manager.save(log);
        return newLog;
    }

}

export default new AssetLogService();