/**
 * Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * SPDX-License-Identifier: MIT
 * 
 * Contributors:
 * Georgios Nikolaidis - Author
 * Vasileios Siopidis - Coauthor
 * Konstantinos Votis - Coauthor
 */

import Joi from '@hapi/joi';
import moment from "moment";
import dbUtil from '../utils/db.util';
import { GaiaXLog } from '../models/gaiaxLog.model';
import { Logform } from 'winston';


const createValidator = Joi.object({
    catalogId: Joi.string().required()
});

class GaiaXLogService {
    async getAll() {
        const connection = await dbUtil.getDefaultConnection();
        const logs = await connection.getRepository(GaiaXLog).find({
            where: {
                isDeleted: false
            }
        });
        return logs;
    }

    async getByDate(queryParams: any) {
        const connection = await dbUtil.getDefaultConnection();
        const query = connection.getRepository(GaiaXLog).createQueryBuilder("m")
            .where("m.isDeleted = :isDeleted").setParameter("isDeleted", false)
            .orderBy("m.createdAt", "DESC");

        if (queryParams.fromDate) {
            query.andWhere("m.createdAt >= :fromDate").setParameter("fromDate", moment(queryParams.fromDate).startOf("d").toDate());
        } else {
            query.andWhere("m.createdAt >= :fromDate").setParameter("fromDate", moment().startOf("year").toDate());
        }
        if (queryParams.toDate) {
            query.andWhere("m.createdAt <= :toDate").setParameter("toDate", moment(queryParams.toDate).endOf("d").toDate());
        } else {
            query.andWhere("m.createdAt <= :toDate").setParameter("toDate", moment().endOf("day").toDate());
        }
        if (queryParams.userId) {
            query.andWhere("m.userId = :userId").setParameter("userId", queryParams.userId);
        }
        const logs = await query.getMany();
        return logs;
    }

    async getById(id: number) {
        const connection = await dbUtil.getDefaultConnection();
        const log = await connection.getRepository(GaiaXLog).findOne({ where: { id } });
        return log;
    }



    async createGaiaXLog(model: any, action: string, username: string) {

        const connection = await dbUtil.getDefaultConnection();
        const log = new GaiaXLog();
        log.userId = model.userId;
        log.userName = username
        log.userToken = model.userToken;
        log.endpointUsed = model.endpointUsed;
        log.createdAt = model.createdAt;
        log.action = action;
        log.providerId = model.providerId;
        log.providerName = model.providerName;
        log.action = action;
        log.participantURL = model.participantURL;
        log.legalName = model.legalName;
        log.dataProductName = model.dataProductName;
        log.openAPIURL = model.openAPIURL;
        log.internalDataProduct_id = model.internalDataProduct_id;
        log.ipAddress = model.ipAddress;
        const newLog = await connection.manager.save(log);
        return newLog;
    }

    async getWithFilters(queryParams: any) {
        const connection = await dbUtil.getDefaultConnection();

        const query = connection.getRepository(GaiaXLog).createQueryBuilder("m")
            .orderBy("m.createdAt", "DESC");

        if (queryParams.fromDate !== 'undefined' && queryParams.fromDate) {
            query.andWhere("m.createdAt >= :fromDate").setParameter("fromDate", moment(queryParams.fromDate).startOf("d").toDate());
        }
        if (queryParams.toDate !== 'undefined' && queryParams.toDate) {
            query.andWhere("m.createdAt <= :toDate").setParameter("toDate", moment(queryParams.toDate).endOf("d").toDate());
        }
        if (queryParams.catalogId) {
            query.andWhere("m.catalogId = :catalogId").setParameter("catalogId", queryParams.catalogId);
        } else if (queryParams.providerName) {
            query.andWhere("m.catalogName = :catalogName").setParameter("catalogName", queryParams.catalogName);
        }
        if (queryParams.ipAddress) {
            query.andWhere("m.ipAddress = :ipAddress").setParameter("ipAddress", queryParams.ipAddress);
        }
        if (queryParams.action !== 'undefined' && queryParams.action) {
            query.andWhere("m.action = :action").setParameter("action", queryParams.action);
        }
        if (queryParams.domain !== 'undefined' && queryParams.domain) {
            query.andWhere("m.domain = :domain").setParameter("domain", queryParams.domain);
        }
        if (queryParams.participantURL !== 'undefined' && queryParams.participantURL) {
            query.andWhere("m.participantURL = :participantURL").setParameter("participantURL", queryParams.participantURL);
        }
        if (queryParams.legalName !== 'undefined' && queryParams.legalName) {
            query.andWhere("m.legalName = :legalName").setParameter("legalName", queryParams.legalName);
        }
        if (queryParams.internalDataProduct_id !== 'undefined' && queryParams.internalDataProduct_id) {
            query.andWhere("m.internalDataProduct_id = :internalDataProduct_id").setParameter("internalDataProduct_id", queryParams.internalDataProduct_id);
        }
        if (queryParams.dataProductName !== 'undefined' && queryParams.dataProductName) {
            query.andWhere("m.dataProductName = :dataProductName").setParameter("dataProductName", queryParams.dataProductName);
        }

        const logs = await query.getMany();
        return logs;
    }

}

export default new GaiaXLogService();