/**
 * Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * SPDX-License-Identifier: MIT
 * 
 * Contributors:
 * Georgios Nikolaidis - Author
 * Vasileios Siopidis - Coauthor
 * Konstantinos Votis - Coauthor
 */

import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, AfterLoad, BeforeInsert, BeforeUpdate } from "typeorm";
import sha256 from 'crypto-js/sha256';
import { logger } from "../utils/logger.util";
import dotenv from "dotenv";

@Entity("app_gaiax_logs")
export class GaiaXLog {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ nullable: true })
    userId: string;

    @Column({ nullable: true })
    userToken: string;

    @Column({ nullable: true })
    userName: string;

    @Column({ nullable: true })
    action?: string;

    @Column({ nullable: true })
    providerId?: string;

    @Column({ nullable: true })
    providerName?: string;

    @Column({ nullable: true })
    endpointUsed?: string;

    @Column({ nullable: true })
    participantURL?: string;

    @Column({ nullable: true })
    legalName?: string;

    @Column({ nullable: true })
    dataProductName?: string;

    @Column({ nullable: true })
    openAPIURL?: string;

    @Column({ nullable: true })
    internalDataProduct_id?: string;

    @Column({ nullable: true })
    type?: string;

    @Column({ nullable: true })
    domain?: string;

    @Column({ nullable: true })
    ipAddress?: string;

    @CreateDateColumn()
    createdAt: Date;

    @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
    timestamp: string;

    @Column({ nullable: false, default: false })
    isDeleted: boolean;

    @Column({ nullable: true })
    modelHash?: string;

    requestHash?: string;

    @AfterLoad()
    checkHash() {

        const nonce = process.env.hashSecret;
        let existingHash = this.modelHash;
        this.modelHash = null;
        let hashDigest = sha256(nonce + JSON.stringify(this.providerId + this.userId + this.timestamp));
        this.modelHash = hashDigest.toString();
        if (existingHash != null && this.modelHash != existingHash) {
            if (this.id > 18) {
                logger.error(` log with id : ` + this.id++ + ' was loaded with different hash');
            }

        }
    }

    @BeforeInsert()
    createHash() {
        const nonce = process.env.hashSecret;
        this.timestamp = new Date().toISOString();
        console.log(this.providerId + " , " + this.userId + " , " + this.timestamp)
        let hashDigest = sha256(nonce + JSON.stringify(this.providerId + this.userId + this.timestamp));
        this.modelHash = hashDigest.toString();

    }

    @BeforeUpdate()
    updateHash() {
        const nonce = process.env.hashSecret;
        this.modelHash = null;
        let hashDigest = sha256(nonce + JSON.stringify(this.providerId + this.userId + this.timestamp));
        this.modelHash = hashDigest.toString();
    }

}