/**
 * Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * SPDX-License-Identifier: MIT
 * 
 * Contributors:
 * Georgios Nikolaidis - Author
 * Vasileios Siopidis - Coauthor
 * Konstantinos Votis - Coauthor
 */

import { NextFunction, Request, Response } from 'express';
import actionLogService from '../services/actionLog.service';
import responseUtil from '../utils/response.util';
import { ApiController } from './api.controller';

class ActionLogController extends ApiController {
    async getAll(req: Request, res: Response, next: NextFunction) {
        try {
            const entities = await actionLogService.getAll();
            return res.json(responseUtil.createSuccessResponse(entities));
        } catch (error) {
            next(error);
        }
    }

    async getById(req: Request, res: Response, next: NextFunction) {
        try {
            let d = "4";
            const entities = await actionLogService.getById(parseInt(d.toString()));
            return res.json(responseUtil.createSuccessResponse(entities));
        } catch (error) {
            next(error);
        }
    }

    async getByDate(req: Request, res: Response, next: NextFunction) {
        try {
            const entities = await actionLogService.getByDate(req.query);
            return res.json(responseUtil.createSuccessResponse(entities));
        } catch (error) {
            next(error);
        }
    }

    async getUserLogs(req: Request, res: Response, next: NextFunction) {
        try {
            const entities = await actionLogService.getByDate(req.query);
            return res.json(responseUtil.createSuccessResponse(entities));
        } catch (error) {
            next(error);
        }
    }

    async create(req: Request, res: Response, next: NextFunction) {
        try {
            const entities = await actionLogService.create(req.body);
            return res.json(responseUtil.createSuccessResponse(entities));
        } catch (error) {
            next(error);
        }
    }

    async createRandomLog(req: Request, res: Response, next: NextFunction) {
        try {
            const log = await actionLogService.createRandom();
            return res.json(responseUtil.createSuccessResponse(log));
        } catch (error) {
            next(error);
        }
    }

    async createLog(req: Request, res: Response, next: NextFunction, action: String) {
        try {
            const log = await actionLogService.createRandom();
            return res.json(responseUtil.createSuccessResponse(log));
        } catch (error) {
            next(error);
        }
    }

}



export default new ActionLogController();