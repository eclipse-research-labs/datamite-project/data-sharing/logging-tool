/**
 * Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * SPDX-License-Identifier: MIT
 * 
 * Contributors:
 * Georgios Nikolaidis - Author
 * Vasileios Siopidis - Coauthor
 * Konstantinos Votis - Coauthor
 */

import { NextFunction, Request, Response } from 'express';
import contractLogService from '../services/contractLog.service';
import responseUtil from '../utils/response.util';
import { ApiController } from './api.controller';
import { ContractLog } from '../models/contractLog.model';
import { ContractLogViewModel } from '../viewModels/contractLog.viewModel';
import { ContractOfferLog } from '../models/contractOfferLog.model';
import { ContractAgreementLog } from '../models/contractAgreementLog.model';

class ContractLogController extends ApiController {

    async getAll(req: Request, res: Response, next: NextFunction) {
        try {
            const entities = await contractLogService.getAll();
            return res.json(responseUtil.createSuccessResponse(entities));
        } catch (error) {
            next(error);
        }
    }

    async getById(req: Request, res: Response, next: NextFunction) {
        try {
            const entities = await contractLogService.getById(Number(req.params.id));
            return res.json(responseUtil.createSuccessResponse(entities));
        } catch (error) {
            next(error);
        }
    }

    async getByDate(req: Request, res: Response, next: NextFunction) {
        try {
            const entities = await contractLogService.getByDate(req.query);
            return res.json(responseUtil.createSuccessResponse(entities));
        } catch (error) {
            next(error);
        }
    }

    async getUserLogs(req: Request, res: Response, next: NextFunction) {
        try {
            const entities = await contractLogService.getByDate(req.query);
            return res.json(responseUtil.createSuccessResponse(entities));
        } catch (error) {
            next(error);
        }
    }

    async getAgreementsPerConsumer(req: Request, res: Response, next: NextFunction) {
        try {
            let model = await contractLogService.getAgreementsPerConsumer(req.query);
            return res.json(responseUtil.createSuccessResponse(model));
        } catch (error) {
            next(error);
        }
    }

    async searchWithFilters(req: Request, res: Response, next: NextFunction) {
        try {
            const entities = await contractLogService.getWithFilters(req.query);
            let resultSet = entities as ContractLog[]
            let model = new ContractLogViewModel();
            model.resultSet = resultSet;
            model.statCounter = resultSet?.length ?? 0;
            return res.json(responseUtil.createSuccessResponse(model));
        } catch (error) {
            next(error);
        }
    }

    async searchContractOffersWithFilters(req: Request, res: Response, next: NextFunction) {
        try {
            const entities = await contractLogService.getContractOffersWithFilters(req.query);
            let resultSet = entities as ContractOfferLog[]
            let model = new ContractLogViewModel();
            model.dataOffers = resultSet;
            model.statCounter = resultSet?.length ?? 0;
            return res.json(responseUtil.createSuccessResponse(model));
        } catch (error) {
            next(error);
        }
    }

    async searchContractAgreementsWithFilters(req: Request, res: Response, next: NextFunction) {
        try {
            const entities = await contractLogService.getContractAgreementsWithFilters(req.query);
            let resultSet = entities as ContractAgreementLog[]
            let model = new ContractLogViewModel();
            model.dataAgreements = resultSet;
            model.statCounter = resultSet?.length ?? 0;
            return res.json(responseUtil.createSuccessResponse(model));
        } catch (error) {
            next(error);
        }
    }

    async create(req: Request, res: Response, next: NextFunction) {
        try {
            const log = await contractLogService.create(req.body);
            //log.createdAt = log.at;
            return res.json(responseUtil.createSuccessResponse(log));
        } catch (error) {
            next(error);
        }
    }

    async update(req: Request, res: Response, next: NextFunction) {
        try {
            const log = await contractLogService.update(req.body);
            return res.json(responseUtil.createSuccessResponse(log));
        } catch (error) {
            next(error);
        }
    }

    async createRandomLog(req: Request, res: Response, next: NextFunction) {
        try {
            const log = await contractLogService.createRandom();
            return res.json(responseUtil.createSuccessResponse(log));
        } catch (error) {
            next(error);
        }
    }

    async createContractLog(req: Request, res: Response, next: NextFunction) {
        try {
            const log = await contractLogService.createContractLog(req.body, 'create');
            return res.json(responseUtil.createSuccessResponse(log));
        } catch (error) {
            next(error);
        }
    }

    async updateContractLog(req: Request, res: Response, next: NextFunction) {
        try {
            const log = await contractLogService.createContractLog(req.body, 'update');
            return res.json(responseUtil.createSuccessResponse(log));
        } catch (error) {
            next(error);
        }
    }

    async deleteContractLog(req: Request, res: Response, next: NextFunction) {
        try {
            const log = await contractLogService.createContractLog(req.body, 'delete');
            return res.json(responseUtil.createSuccessResponse(log));
        } catch (error) {
            next(error);
        }
    }

    async negotiateContractLog(req: Request, res: Response, next: NextFunction) {
        try {
            const log = await contractLogService.createContractLog(req.body, 'negotiate');
            return res.json(responseUtil.createSuccessResponse(log));
        } catch (error) {
            next(error);
        }
    }

    async rejectContractLog(req: Request, res: Response, next: NextFunction) {
        try {
            const log = await contractLogService.createContractLog(req.body, 'reject');
            return res.json(responseUtil.createSuccessResponse(log));
        } catch (error) {
            next(error);
        }
    }

    async createContractOfferLog(req: Request, res: Response, next: NextFunction) {
        try {
            const log = await contractLogService.createContractOfferLog(req.body, 'create');
            return res.json(responseUtil.createSuccessResponse(log));
        } catch (error) {
            next(error);
        }
    }

    async updateContractOfferLog(req: Request, res: Response, next: NextFunction) {
        try {
            const log = await contractLogService.createContractOfferLog(req.body, 'update');
            return res.json(responseUtil.createSuccessResponse(log));
        } catch (error) {
            next(error);
        }
    }

    async deleteContractOfferLog(req: Request, res: Response, next: NextFunction) {
        try {
            const log = await contractLogService.createContractOfferLog(req.body, 'delete');
            return res.json(responseUtil.createSuccessResponse(log));
        } catch (error) {
            next(error);
        }
    }

    async createContractAgreementLog(req: Request, res: Response, next: NextFunction) {
        try {
            const log = await contractLogService.createContractAgreementLog(req.body, 'create');
            return res.json(responseUtil.createSuccessResponse(log));
        } catch (error) {
            next(error);
        }
    }

    async updateContractAgreementLog(req: Request, res: Response, next: NextFunction) {
        try {
            const log = await contractLogService.createContractAgreementLog(req.body, 'update');
            return res.json(responseUtil.createSuccessResponse(log));
        } catch (error) {
            next(error);
        }
    }

    async deleteContractAgreementLog(req: Request, res: Response, next: NextFunction) {
        try {
            const log = await contractLogService.createContractAgreementLog(req.body, 'delete');
            return res.json(responseUtil.createSuccessResponse(log));
        } catch (error) {
            next(error);
        }
    }


}

export default new ContractLogController();