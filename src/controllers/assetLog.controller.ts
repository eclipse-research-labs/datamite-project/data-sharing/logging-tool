/**
 * Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * SPDX-License-Identifier: MIT
 * 
 * Contributors:
 * Georgios Nikolaidis - Author
 * Vasileios Siopidis - Coauthor
 * Konstantinos Votis - Coauthor
 */

import { NextFunction, Request, Response } from 'express';
import assetLogService from '../services/assetLog.service';
import responseUtil from '../utils/response.util';
import { ApiController } from './api.controller';
import { AssetLogViewModel } from '../viewModels/assetLog.viewModel';
import { AssetLog } from '../models/assetLog.model';
import { unUsedAssetsViewModel } from '../viewModels/unUsedAssetsViewModel';


class AssetLogController extends ApiController {

    async getAll(req: Request, res: Response, next: NextFunction) {
        try {
            const entities = await assetLogService.getAll();
            return res.json(responseUtil.createSuccessResponse(entities));
        } catch (error) {
            next(error);
        }
    }

    async getById(req: Request, res: Response, next: NextFunction) {
        try {
            const entities = await assetLogService.getById(Number(req.params.id));
            return res.json(responseUtil.createSuccessResponse(entities));
        } catch (error) {
            next(error);
        }
    }

    async getByDate(req: Request, res: Response, next: NextFunction) {
        try {
            const entities = await assetLogService.getByDate(req.query);
            return res.json(responseUtil.createSuccessResponse(entities));
        } catch (error) {
            next(error);
        }
    }

    async getUserLogs(req: Request, res: Response, next: NextFunction) {
        try {
            const entities = await assetLogService.getByDate(req.query);
            return res.json(responseUtil.createSuccessResponse(entities));
        } catch (error) {
            next(error);
        }
    }

    async getByAssetId(req: Request, res: Response, next: NextFunction) {
        try {
            let entities = await assetLogService.getByAssetId(req.query);
            let resultSet = entities as AssetLog[];
            let model = new AssetLogViewModel(resultSet);
            return res.json(responseUtil.createSuccessResponse(model));
        } catch (error) {
            next(error);
        }
    }

    async searchWithFilters(req: Request, res: Response, next: NextFunction) {
        try {
            let entities = await assetLogService.getWithFilters(req.query);
            let resultSet = entities as AssetLog[];
            let model = new AssetLogViewModel(resultSet);
            return res.json(responseUtil.createSuccessResponse(model));
        } catch (error) {
            next(error);
        }
    }
    async getUnusedAssets(req: Request, res: Response, next: NextFunction) {
        try {
            let model = await assetLogService.getUnusedAssets(req.query);
            return res.json(responseUtil.createSuccessResponse(model));
        } catch (error) {
            next(error);
        }
    }

    async getIncomingRequests(req: Request, res: Response, next: NextFunction) {
        try {

            let model = await assetLogService.getIncomingRequests(req.query);
            return res.json(responseUtil.createSuccessResponse(model));
        } catch (error) {
            next(error);
        }
    }

    async getPopularAssets(req: Request, res: Response, next: NextFunction) {
        try {

            let model = await assetLogService.getPopularAssets(req.query);
            return res.json(responseUtil.createSuccessResponse(model));
        } catch (error) {
            next(error);
        }
    }

    async create(req: Request, res: Response, next: NextFunction) {
        try {
            const log = await assetLogService.create(req.body);
            return res.json(responseUtil.createSuccessResponse(log));
        } catch (error) {
            next(error);
        }
    }

    async update(req: Request, res: Response, next: NextFunction) {
        try {
            const log = await assetLogService.update(req.body);
            return res.json(responseUtil.createSuccessResponse(log));
        } catch (error) {
            next(error);
        }
    }

    async transfer(req: Request, res: Response, next: NextFunction) {
        try {
            const log = await assetLogService.createAssetLog(req.body, 'transfer');
            return res.json(responseUtil.createSuccessResponse(log));
        } catch (error) {
            next(error);
        }
    }


    async createRandomLog(req: Request, res: Response, next: NextFunction) {
        try {
            const log = await assetLogService.createRandom();
            return res.json(responseUtil.createSuccessResponse(log));
        } catch (error) {
            next(error);
        }
    }

    async createAssetLog(req: Request, res: Response, next: NextFunction) {
        try {
            const log = await assetLogService.createAssetLog(req.body, 'create');
            return res.json(responseUtil.createSuccessResponse(log));
        } catch (error) {
            next(error);
        }
    }

    async updateAssetLog(req: Request, res: Response, next: NextFunction) {
        try {
            const log = await assetLogService.createAssetLog(req.body, 'update');
            return res.json(responseUtil.createSuccessResponse(log));
        } catch (error) {
            next(error);
        }
    }

    async deleteAssetLog(req: Request, res: Response, next: NextFunction) {
        try {
            const log = await assetLogService.createAssetLog(req.body, 'delete');
            return res.json(responseUtil.createSuccessResponse(log));
        } catch (error) {
            next(error);
        }
    }

    async transferAssetLog(req: Request, res: Response, next: NextFunction) {
        try {
            const log = await assetLogService.createAssetLog(req.body, 'transfer');
            return res.json(responseUtil.createSuccessResponse(log));
        } catch (error) {
            next(error);
        }
    }

    async transferWithHashedData(req: Request, res: Response, next: NextFunction) {
        try {
            const log = await assetLogService.createAssetLog(req.body, 'transferWithHashedData');
            if (log === 'errorhash') {
                return res.json(responseUtil.createErrorResponse("data was altered"));
            }
            return res.json(responseUtil.createSuccessResponse(log));
        } catch (error) {
            next(error);
        }
    }

    async publishAssetLog(req: Request, res: Response, next: NextFunction) {
        try {
            const log = await assetLogService.createAssetLog(req.body, 'publish');
            return res.json(responseUtil.createSuccessResponse(log));
        } catch (error) {
            next(error);
        }
    }

}



export default new AssetLogController();