/**
 * Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * SPDX-License-Identifier: MIT
 * 
 * Contributors:
 * Georgios Nikolaidis - Author
 * Vasileios Siopidis - Coauthor
 * Konstantinos Votis - Coauthor
 */

import { NextFunction, Request, Response } from 'express';
import gaiaXLogService from '../services/gaiaxLog.service';
import responseUtil from '../utils/response.util';
import { ApiController } from './api.controller';
import { GaiaXLogViewModel } from '../viewModels/gaiaxLog.viewModel';
import { GaiaXLog } from '../models/gaiaxLog.model';
import { Keycloak } from "keycloak-backend"
import { KEYCLOAK_URL } from '../utils/config.util';
import * as jwt from "jsonwebtoken";
import * as jwtJsDecode from 'jwt-js-decode';
import { logger } from '../utils/logger.util';

class GaiaXLogController extends ApiController {

    async getAll(req: Request, res: Response, next: NextFunction) {
        try {
            const entities = await gaiaXLogService.getAll();
            return res.json(responseUtil.createSuccessResponse(entities));
        } catch (error) {
            next(error);
        }
    }

    async getById(req: Request, res: Response, next: NextFunction) {
        try {
            const entities = await gaiaXLogService.getById(Number(req.params.id));
            return res.json(responseUtil.createSuccessResponse(entities));
        } catch (error) {
            next(error);
        }
    }

    async getByDate(req: Request, res: Response, next: NextFunction) {
        try {
            const entities = await gaiaXLogService.getByDate(req.query);
            return res.json(responseUtil.createSuccessResponse(entities));
        } catch (error) {
            next(error);
        }
    }

    async searchWithFilters(req: Request, res: Response, next: NextFunction) {
        try {
            let entities = await gaiaXLogService.getWithFilters(req.query);
            let resultSet = entities as GaiaXLog[]
            let model = new GaiaXLogViewModel(resultSet);
            return res.json(responseUtil.createSuccessResponse(model));
        } catch (error) {
            next(error);
        }
    }

    async publishDataProductLog(req: Request, res: Response, next: NextFunction) {
        try {
            const log = await gaiaXLogService.createGaiaXLog(req.body, 'publishDataProductLog', await this.GetUsernameFromToken(req));
            return res.json(responseUtil.createSuccessResponse(log));
        } catch (error) {
            next(error);
        }
    }

    async createdid(req: Request, res: Response, next: NextFunction) {
        try {
            const log = await gaiaXLogService.createGaiaXLog(req.body, 'createdid', await this.GetUsernameFromToken(req));
            return res.json(responseUtil.createSuccessResponse(log));
        } catch (error) {
            next(error);
        }
    }

    async createParticipant(req: Request, res: Response, next: NextFunction) {
        try {
            const log = await gaiaXLogService.createGaiaXLog(req.body, 'createParticipant', await this.GetUsernameFromToken(req));
            return res.json(responseUtil.createSuccessResponse(log));
        } catch (error) {
            next(error);
        }
    }


    async exportGaiaxModel(req: Request, res: Response, next: NextFunction) {
        try {
            const log = await gaiaXLogService.createGaiaXLog(req.body, 'exportGaiaxModel', await this.GetUsernameFromToken(req));
            return res.json(responseUtil.createSuccessResponse(log));
        } catch (error) {
            next(error);
        }
    }


    async GetUsernameFromToken(req: any) {
        const keycloak = new Keycloak({
            "realm": "datamite",
            "keycloak_base_url": KEYCLOAK_URL,// "http://160.40.52.10:9093",
            "client_id": "datamite_client_auth_test",
            "username": "testUser",
            "password": "testUser1!",
            "is_legacy_endpoint": false
        });
        let username = "";
        try {
            if (req.headers && req.headers.authtoken) {
                let token = req.headers.authtoken

                const options: jwt.VerifyOptions = {
                    algorithms: ["RS256"],
                };

                let res1 = jwtJsDecode.decode(token);
                if (res1.payload && res1.payload.realm_access && res1.payload.realm_access.roles && res1.payload.realm_access.roles.length > 0) {

                    const decodedToken = await keycloak.jwt.decode(token)

                    if (decodedToken.isExpired()) {
                        return username;
                    } else {
                        username = decodedToken.content['preferred_username']
                    }
                }
            }
        } catch (error) {
            logger.error(error.message);
        }
        return username;
    }

}

export default new GaiaXLogController();