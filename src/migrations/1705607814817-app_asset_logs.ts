import {MigrationInterface, QueryRunner} from "typeorm";

export class appActionLogs1705607814817 implements MigrationInterface {
    name = 'appActionLogs1705607814817'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`app_asset_logs\` (\`id\` int NOT NULL AUTO_INCREMENT, \`userId\` int NOT NULL, \`action\` varchar(255) NULL, \`notes\` varchar(255) NULL, \`createdAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`timestamp\` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, \`isDeleted\` tinyint NOT NULL DEFAULT 0, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE \`app_asset_logs\``);
    }

}
