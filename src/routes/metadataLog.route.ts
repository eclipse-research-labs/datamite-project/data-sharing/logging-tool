/**
 * Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * SPDX-License-Identifier: MIT
 * 
 * Contributors:
 * Georgios Nikolaidis - Author
 * Vasileios Siopidis - Coauthor
 * Konstantinos Votis - Coauthor
 */

import * as express from 'express';
import { Request, Response, NextFunction } from 'express';
import { IRouting, ImportedRoute } from './routing.interface';

import metadataLogController from '../controllers/metadataLog.controller';


@ImportedRoute.register
class MetadataLogsRoutes implements IRouting {
    prefix = '/metadataLog';

    register(app: express.Application) {

        app.get(`/metadataLog/getall`, (req: Request, res: Response, next: NextFunction) => {
            return metadataLogController.getAll(req, res, next);
        });

        app.get(`/metadataLog/getByDate`, (req: Request, res: Response, next: NextFunction) => {
            return metadataLogController.getByDate(req, res, next);
        });

        app.post(`/metadataLog/publish`, (req: Request, res: Response, next: NextFunction) => {
            return metadataLogController.publishMetadataLog(req, res, next);
        });

        app.get(`/metadataLog/searchWithFilters`, (req: Request, res: Response, next: NextFunction) => {
            return metadataLogController.searchWithFilters(req, res, next);
        });




    }
}
export default new MetadataLogsRoutes();