/**
 * Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * SPDX-License-Identifier: MIT
 * 
 * Contributors:
 * Georgios Nikolaidis - Author
 * Vasileios Siopidis - Coauthor
 * Konstantinos Votis - Coauthor
 */

import * as express from 'express';
import { Request, Response, NextFunction } from 'express';
import { IRouting, ImportedRoute } from './routing.interface';

import contractLogController from '../controllers/contractLog.controller';


@ImportedRoute.register
class ContractLogsRoutes implements IRouting {
    prefix = '/contractLog';

    register(app: express.Application) {

        app.get(`/contractLog/getall`, (req: Request, res: Response, next: NextFunction) => {
            return contractLogController.getAll(req, res, next);
        });

        app.get(`/contractLog/getByDate`, (req: Request, res: Response, next: NextFunction) => {
            return contractLogController.getByDate(req, res, next);
        });

        app.get(`/contractLog/getByTransferDate`, (req: Request, res: Response, next: NextFunction) => {
            return contractLogController.getByDate(req, res, next);
        });

        app.get(`/contractLog/getAgreementsPerConsumer`, (req: Request, res: Response, next: NextFunction) => {
            return contractLogController.getAgreementsPerConsumer(req, res, next);
        });

        app.get(`/contractLog/getByUser`, (req: Request, res: Response, next: NextFunction) => {
            return contractLogController.getUserLogs(req, res, next);
        });
        app.get(`/contractLog/searchWithFilters`, (req: Request, res: Response, next: NextFunction) => {
            return contractLogController.searchWithFilters(req, res, next);
        });

        app.post(`/contractLog/createRandom`, (req: Request, res: Response, next: NextFunction) => {
            return contractLogController.createRandomLog(req, res, next);
        });

        app.post(`/contractLog/create`, (req: Request, res: Response, next: NextFunction) => {
            return contractLogController.createContractLog(req, res, next);
        });
        app.post(`/contractLog/update`, (req: Request, res: Response, next: NextFunction) => {
            return contractLogController.updateContractLog(req, res, next);
        });

        app.post(`/contractLog/delete`, (req: Request, res: Response, next: NextFunction) => {
            return contractLogController.deleteContractLog(req, res, next);
        });

        app.post(`/contractLog/negotiate`, (req: Request, res: Response, next: NextFunction) => {
            return contractLogController.negotiateContractLog(req, res, next);
        });

        app.post(`/contractLog/reject`, (req: Request, res: Response, next: NextFunction) => {
            return contractLogController.rejectContractLog(req, res, next);
        });

        app.post(`/contractOfferLog/create`, (req: Request, res: Response, next: NextFunction) => {
            return contractLogController.createContractOfferLog(req, res, next);
        });

        app.post(`/contractOfferLog/update`, (req: Request, res: Response, next: NextFunction) => {
            return contractLogController.updateContractOfferLog(req, res, next);
        });

        app.post(`/contractOfferLog/delete`, (req: Request, res: Response, next: NextFunction) => {
            return contractLogController.deleteContractOfferLog(req, res, next);
        });

        app.post(`/contractAgreementLog/create`, (req: Request, res: Response, next: NextFunction) => {
            return contractLogController.createContractAgreementLog(req, res, next);
        });

        app.post(`/contractAgreementLog/update`, (req: Request, res: Response, next: NextFunction) => {
            return contractLogController.updateContractAgreementLog(req, res, next);
        });

        app.post(`/contractAgreementLog/delete`, (req: Request, res: Response, next: NextFunction) => {
            return contractLogController.deleteContractAgreementLog(req, res, next);
        });

        app.get(`/contractOfferLog/searchWithFilters`, (req: Request, res: Response, next: NextFunction) => {
            return contractLogController.searchContractOffersWithFilters(req, res, next);
        });

        app.get(`/contractAgreementLog/searchWithFilters`, (req: Request, res: Response, next: NextFunction) => {
            return contractLogController.searchContractAgreementsWithFilters(req, res, next);
        });

    }
}
export default new ContractLogsRoutes();