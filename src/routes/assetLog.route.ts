/**
 * Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * SPDX-License-Identifier: MIT
 * 
 * Contributors:
 * Georgios Nikolaidis - Author
 * Vasileios Siopidis - Coauthor
 * Konstantinos Votis - Coauthor
 */

import * as express from 'express';
import { Request, Response, NextFunction } from 'express';
import { IRouting, ImportedRoute } from './routing.interface';

import assetLogController from '../controllers/assetLog.controller';


@ImportedRoute.register
class AssetLogsRoutes implements IRouting {
    prefix = '/assetLog';

    register(app: express.Application) {

        app.get(`/assetLog/getall`, (req: Request, res: Response, next: NextFunction) => {
            return assetLogController.getAll(req, res, next);
        });

        app.get(`/assetLog/getByAssetId`, (req: Request, res: Response, next: NextFunction) => {
            return assetLogController.getByAssetId(req, res, next);
        });

        app.get(`/assetLog/getByDate`, (req: Request, res: Response, next: NextFunction) => {
            return assetLogController.getByDate(req, res, next);
        });

        app.get(`/assetLog/getByTransferDate`, (req: Request, res: Response, next: NextFunction) => {
            return assetLogController.getByDate(req, res, next);
        });

        app.get(`/assetLog/getByUser`, (req: Request, res: Response, next: NextFunction) => {
            return assetLogController.getUserLogs(req, res, next);
        });

        app.get(`/assetLog/searchWithFilters`, (req: Request, res: Response, next: NextFunction) => {
            return assetLogController.searchWithFilters(req, res, next);
        });

        app.post(`/assetLog/createRandom`, (req: Request, res: Response, next: NextFunction) => {
            return assetLogController.createRandomLog(req, res, next);
        });

        app.post(`/assetLog/create`, (req: Request, res: Response, next: NextFunction) => {
            return assetLogController.createAssetLog(req, res, next);
        });

        app.post('/assetLog/update', (req: Request, res: Response, next: NextFunction) => {
            return assetLogController.updateAssetLog(req, res, next);
        });

        app.post(`/assetLog/delete`, (req: Request, res: Response, next: NextFunction) => {
            return assetLogController.deleteAssetLog(req, res, next);
        });

        app.post(`/assetLog/transfer`, (req: Request, res: Response, next: NextFunction) => {
            return assetLogController.transfer(req, res, next);
        });

        app.post(`/assetLog/transferWithHashedData`, (req: Request, res: Response, next: NextFunction) => {
            return assetLogController.transferWithHashedData(req, res, next);
        });

        app.post(`/assetLog/publish`, (req: Request, res: Response, next: NextFunction) => {
            return assetLogController.transferAssetLog(req, res, next);
        });

        app.get(`/assetLog/getUnusedAssets`, (req: Request, res: Response, next: NextFunction) => {
            return assetLogController.getUnusedAssets(req, res, next);
        });

        app.get(`/assetLog/getIncomingRequests`, (req: Request, res: Response, next: NextFunction) => {
            return assetLogController.getIncomingRequests(req, res, next);
        });

        app.get(`/assetLog/getPopularAssets`, (req: Request, res: Response, next: NextFunction) => {
            return assetLogController.getPopularAssets(req, res, next);
        });


    }
}
export default new AssetLogsRoutes();