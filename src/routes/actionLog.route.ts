/**
 * Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * SPDX-License-Identifier: MIT
 * 
 * Contributors:
 * Georgios Nikolaidis - Author
 * Vasileios Siopidis - Coauthor
 * Konstantinos Votis - Coauthor
 */

import * as express from 'express';
import { Request, Response, NextFunction } from 'express';
import { IRouting, ImportedRoute } from './routing.interface';

import actionLogController from '../controllers/actionLog.controller';


@ImportedRoute.register
class ActionsLogsRoutes implements IRouting {
    prefix = '/actionlog';

    register(app: express.Application) {

        app.get(`/actionlog/getall`, (req: Request, res: Response, next: NextFunction) => {
            return actionLogController.getAll(req, res, next);
        });

        app.get(`/actionlog/getById`, (req: Request, res: Response, next: NextFunction) => {
            return actionLogController.getById(req, res, next);
        });

        app.get(`/actionlog/getByDate`, (req: Request, res: Response, next: NextFunction) => {
            return actionLogController.getByDate(req, res, next);
        });

        app.get(`/actionlog/getByUser`, (req: Request, res: Response, next: NextFunction) => {
            return actionLogController.getUserLogs(req, res, next);
        });

        app.post(`/actionlog/createRandom`, (req: Request, res: Response, next: NextFunction) => {
            return actionLogController.createRandomLog(req, res, next);
        });

        app.post(`/actionlog/create`, (req: Request, res: Response, next: NextFunction) => {
            return actionLogController.createLog(req, res, next, "create");
        });

        app.post(`/actionlog/update`, (req: Request, res: Response, next: NextFunction) => {
            return actionLogController.createLog(req, res, next, "update");
        });

        app.post(`/actionlog/delete`, (req: Request, res: Response, next: NextFunction) => {
            return actionLogController.createLog(req, res, next, "delete");
        });


    }
}
export default new ActionsLogsRoutes();