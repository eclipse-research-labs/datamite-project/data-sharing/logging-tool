/**
 * Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * SPDX-License-Identifier: MIT
 * 
 * Contributors:
 * Georgios Nikolaidis - Author
 * Vasileios Siopidis - Coauthor
 * Konstantinos Votis - Coauthor
 */

import express from "express";
import dotenv, { config } from "dotenv";
import "reflect-metadata";
import * as routes from "./routes";
import { logger } from "./utils/logger.util";
import DbUtil from "./utils/db.util";
import { handle } from "./utils/error-handling.util";
import bodyParser from "body-parser";
import cors from "cors";
import helmet from "helmet";
import fileUpload from "express-fileupload";
import * as jwt from "jsonwebtoken";
import * as jwtJsDecode from 'jwt-js-decode';
dotenv.config({ path: ".env" });
import { helpers } from "./utils/helpers.util";
import responseUtil from './utils/response.util';
import { Keycloak } from "keycloak-backend"
import { KEYCLOAK_URL } from './utils/config.util';

const swaggerUi = require('swagger-ui-express');


// tslint:disable-next-line: no-var-requires


declare module "express" {
  interface Request {
    //user?: User;
  }
}

declare global {
  namespace CookieSessionInterfaces {
    interface CookieSessionObject {
      cartId: number;
    }
  }
}

// Initialize environment
dotenv.config();

// Initialize app
let app = express();
app.use(
  helmet({
    contentSecurityPolicy: {
      directives: {
        ...helmet.contentSecurityPolicy.getDefaultDirectives()
      }
    }
  })
);
const port = process.env.SERVER_PORT;

app.use(
  fileUpload({
    defCharset: "utf8",
    defParamCharset: "utf8"
  })
);


const corsOptions: cors.CorsOptions = {
  origin: "*",
  methods: ["GET", "POST", "PUT", "OPTIONS", "DELETE"],
  allowedHeaders: "*",
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};
app.use(cors(corsOptions));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

app.options("*", cors());

// Initialize database connections
DbUtil.init();

// Middleware
app.use(async (req, res, next) => {

  let result = false;
  let requireAuth = false;
  let dataProviderEndpoints = helpers.getAppDataProviderEndpoints();
  if (req.method && req.method.toLowerCase() == "get" && req.url) {
    for (let i = 0; i < dataProviderEndpoints.length; i++) {
      if (req.url.includes(dataProviderEndpoints[i])) {
        requireAuth = true;
        result = await AuthorizeAsDataProvider(req);
        break;
      }
    }
  }

  let appProviderEndpoints = helpers.getAppEndpoints();
  if (req.method && req.method.toLowerCase() == "post" && req.url) {
    for (let i = 0; i < appProviderEndpoints.length; i++) {
      if (req.url.includes(appProviderEndpoints[i])) {
        requireAuth = true;
        result = await AuthorizeWithToken(req);
        break;
      }
    }
  }

  if (requireAuth === true && result == false) {
    try {
      if (res.writable == true) {
        return res.status(401).send("Unauthorized");
      }
    } catch (error) {
      logger.error(error.message);
    }
  }

  next();

});

const swaggerFile = require('../swagger_output.json');
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerFile));

// Register all routes
routes.registerRoutes(app);

// Handle un-handled errors
handle(app);


// start the express server
const server = app.listen(port, () => {
  logger.info(`server started at http://localhost:${port}`);
});

//socketUtil.init(server);

const keycloak = new Keycloak({
  "realm": "datamite",
  "keycloak_base_url": KEYCLOAK_URL,// "http://160.40.52.10:9093",
  "client_id": "datamite_client_auth_test",
  "username": "testUser",
  "password": "testUser1!",
  "is_legacy_endpoint": false
});
async function AuthorizeAsDataProvider(req: any) {
  let hasRole = false;
  try {
    if (req.headers && req.headers.authtoken) {
      let token = req.headers.authtoken

      const options: jwt.VerifyOptions = {
        algorithms: ["RS256"],
      };

      let res1 = jwtJsDecode.decode(token);
      if (res1.payload && res1.payload.realm_access && res1.payload.realm_access.roles && res1.payload.realm_access.roles.length > 0) {

        const decodedToken = await keycloak.jwt.decode(token)
        if (decodedToken.isExpired()) {
          hasRole = false;
        } else {
          hasRole = true;
        }
        if (hasRole == true && decodedToken.hasRealmRole('Data Provider')) {
          hasRole = true;
        }
        else {
          hasRole = false;
        }
      }
    }
  } catch (error) {
    hasRole = false;
    logger.error(error.message);
  }
  return hasRole;
}

async function AuthorizeWithToken(req: any) {
  let hasToken = false;
  if (req.headers && req.headers.authtoken) {
    let token = req.headers.authtoken

    const options: jwt.VerifyOptions = {
      algorithms: ["RS256"],
    };

    try {
      const decodedToken = await keycloak.jwt.decode(token)
      if (decodedToken.isExpired()) {
        hasToken = false;
      } else {
        hasToken = true;
      }

    } catch (error) {
      hasToken = false;
      logger.error(error.message);
    }
  }
  return hasToken;
}


