/**
 * Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * SPDX-License-Identifier: MIT
 * 
 * Contributors:
 * Georgios Nikolaidis - Author
 * Vasileios Siopidis - Coauthor
 * Konstantinos Votis - Coauthor
 */

import * as fs from "fs";
import * as jwt from "jsonwebtoken";
import { logger } from "./logger.util";
import moment from "moment";
import stripHtml from "string-strip-html";
import { Connection, EntityTarget, SelectQueryBuilder } from "typeorm";


class Helpers {
  public deserializeQueryString(str: string) {
    const variables = str.split("&");
    const ob: any = {};

    variables.forEach((x) => {
      ob[x.split("=")[0]] = x.split("=")[1];
    });

    return ob;
  }

  public deserializeUrl(str: string) {
    const variables = str.split("/");
    const ob: any = {};

    variables.forEach((x) => {
      ob[x.split("=")[0]] = x.split("=")[1];
    });

    return ob;
  }



  public randomString(length: number = 10) {
    let result = "";
    const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  public randomCapitalString(length: number = 10) {
    let result = "";
    const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  public randomNumber(length: number = 10) {
    let result = "";
    const characters = "1234567890";
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return Number(result);
  }

  public createAppointmentVideoCallId() {
    return new Date().getTime().toString() + "_" + this.randomString(24);
  }

  public arraysAreEqual(a: any[], b: any[]) {
    if (a === b) return true;
    if (a == null || b == null) return false;
    if (a.length !== b.length) return false;

    const arrA = a.slice().sort();
    const arrB = b.slice().sort();

    for (let i = 0; i < arrA.length; ++i) {
      if (arrA[i] !== arrB[i]) return false;
    }
    return true;
  }

  public getNonHtmlSubString(text: string, numberOfChars: number, applyDots: boolean = false) {
    const nonHtml = stripHtml(text).result;

    let sub = nonHtml.substring(0, nonHtml.length < numberOfChars ? nonHtml.length : numberOfChars);

    if (nonHtml.length >= numberOfChars) {
      sub += "..";
    }

    return sub;
  }

  public getUniqueStrings(value: any, index: any, self: any) {
    return self.indexOf(value) === index;
  }

  public treeify(list: any[], idAttr: any, parentAttr: any, childrenAttr: any) {
    if (!idAttr) idAttr = "id";
    if (!parentAttr) parentAttr = "parent";
    if (!childrenAttr) childrenAttr = "children";

    const treeList: any[] = [];
    const lookup: any = {};
    list.forEach((obj) => {
      lookup[obj[idAttr]] = obj;
      obj[childrenAttr] = [];
    });
    list.forEach((obj) => {
      if (obj[parentAttr] != null) {
        lookup[obj[parentAttr]][childrenAttr].push(obj);
      } else {
        treeList.push(obj);
      }
    });
    return treeList;
  }

  public isImageFile(filename: string) {
    try {
      const extension = filename.split(".")[filename.split(".").length - 1].toLowerCase();
      if (
        extension === "png" ||
        extension === "jpg" ||
        extension === "jpeg" ||
        extension === "jpeg" ||
        extension === "tiff" ||
        extension === "gif" ||
        extension === "bmp" ||
        extension === "webp"
      ) {
        return true;
      }
    } catch (err) {
      logger.error("error on isImageFile function. filenname to check: " + filename);
    }
    return false;
  }

  public isPdfFile(filename: string) {
    try {
      const extension = filename.split(".")[filename.split(".").length - 1].toLowerCase();
      if (extension === "pdf" || extension === "PDF") {
        return true;
      }
    } catch (err) {
      logger.error("error on isImageFile function. filenname to check: " + filename);
    }
    return false;
  }

  getQty(str: string) {
    if (str.indexOf("G") > -1 && str.indexOf("KG") === -1) {
      return parseFloat(str) / 1000;
    } else if (str.indexOf("KG") > -1) {
      return parseFloat(str.replace(",", "."));
    }
  }

  hasFreeShipping() {
    const d = moment().format("YYYY-MM");
    if (process.env.NO_SHIPMENT_FEES && process.env.NO_SHIPMENT_FEES.indexOf(d) > -1) {
      return true;
    }
    return false;
  }

  getShippingCostsWithVAT(amount: number) {
    const d = moment().format("YYYY-MM");
    if (process.env.NO_SHIPMENT_FEES && process.env.NO_SHIPMENT_FEES.indexOf(d) > -1) {
      return 0;
    }
    return amount;
  }

  getAntikatavoliCostsWithVat() {
    // const d = moment().format("YYYY-MM");
    // if (NO_SHIPMENT_FEES.indexOf(d) > -1) {
    //   return 0;
    // }
    return Number(process.env.ANTIKATAVOLI_COST);
  }

  getShippingCosts(amount: number) {
    const d = moment().format("YYYY-MM");
    if (process.env.NO_SHIPMENT_FEES && process.env.NO_SHIPMENT_FEES.indexOf(d) > -1) {
      return (0).toFixed(2);
    }
    return (amount / 1.24).toFixed(2);
  }

  getAntikatavoliCosts() {
    // const d = moment().format("YYYY-MM");
    // if (NO_SHIPMENT_FEES.indexOf(d) > -1) {
    //   return (0).toFixed(2);
    // }
    return (Number(process.env.ANTIKATAVOLI_COST) / 1.24).toFixed(2);
  }

  createDefaultSearchQuery<T>(entityType: EntityTarget<T>, searchOb: any, query: SelectQueryBuilder<T>, connection: Connection) {
    const metadata = connection.getMetadata(entityType);
    for (const prop in searchOb) {
      if (!searchOb.hasOwnProperty(prop)) continue;
      if (metadata.columns.find((z) => z.propertyName === prop)) {
        if (searchOb[prop] !== undefined && searchOb[prop] !== null && searchOb[prop] !== "") {
          const alias = query.expressionMap.aliases.find((z) => z.target === entityType);
          query = query.andWhere(`${alias.name}.${prop} like :${prop}`).setParameter(prop, `%${searchOb[prop]}%`);
        }
      } else {
        if (metadata.relations.find((z) => z.propertyName === prop)) {
          const foreignKey = metadata.relations.find((z) => z.propertyName === prop);
          this.createDefaultSearchQuery(foreignKey.type, searchOb[prop], query, connection);
        }
      }
    }
    if (searchOb.sortField) {
      const aliasName = this.getAliasOfObjectPath<T>(searchOb.sortField, connection, entityType, query);
      if (aliasName) {
        query = query.orderBy(aliasName.alias + "." + aliasName.propertyName, searchOb.sortOrder === 1 ? "ASC" : "DESC");
      }
    }
    if (searchOb.limit !== undefined) {
      query = query.take(Number(searchOb.limit));
    }
    if (searchOb.offset !== undefined) {
      query = query.skip(Number(searchOb.offset));
    }

    return query;
  }

  getAppEndpoints() {
    let endpoints = []
    endpoints.push("assetLog/create");
    endpoints.push("assetLog/update");
    endpoints.push("assetLog/delete");
    endpoints.push("assetLog/transfer");
    endpoints.push("assetLog/transferWithHashedData");
    endpoints.push("assetLog/publish");
    endpoints.push("policyLog/create");
    endpoints.push("policyLog/update");
    endpoints.push("policyLog/delete");
    endpoints.push("contractLog/create");
    endpoints.push("contractLog/update");
    endpoints.push("contractLog/delete");
    endpoints.push("contractOfferLog/create");
    endpoints.push("contractOfferLog/update");
    endpoints.push("contractOfferLog/delete");
    endpoints.push("contractAgreementLog/create");
    endpoints.push("contractAgreementLog/update");
    endpoints.push("contractAgreementLog/delete");
    endpoints.push("contractLog/negotiate");
    endpoints.push("contractLog/reject");
    endpoints.push("metadataLog/publish");
    endpoints.push("gaiaxlogs/createdid");
    endpoints.push("gaiaxlogs/createParticipant");
    endpoints.push("gaiaxlogs/exportGaiaxModel");
    endpoints.push("gaiaxlogs/publishDataProduct");
    return endpoints;
  }

  getAppDataProviderEndpoints() {
    let endpoints = []
    endpoints.push("assetLog/getall");
    endpoints.push("assetLog/getIncomingRequests");
    endpoints.push("assetLog/getByDate");
    endpoints.push("assetLog/getByAssetId");
    endpoints.push("assetLog/getByTransferDate");
    endpoints.push("assetLog/searchWithFilters");
    endpoints.push("assetLog/getUnusedAssets");
    endpoints.push("assetLog/getPopularAssets");
    endpoints.push("policyLog/getall");
    endpoints.push("policyLog/getByPolicyId");
    endpoints.push("policyLog/getByDate");
    endpoints.push("policyLog/getByTransferDate");
    endpoints.push("metadataLog/getall");
    endpoints.push("metadataLog/getByDate");
    endpoints.push("metadataLog/searchWithFilters");
    endpoints.push("contractLog/getall");
    endpoints.push("contractLog/getByDate");
    endpoints.push("contractLog/getByTransferDate");
    endpoints.push("metadataLog/searchWithFilters");
    endpoints.push("contractLog/getAgreementsPerConsumer");
    endpoints.push("contractLog/searchWithFilters");
    endpoints.push("contractOfferLog/searchWithFilters");
    endpoints.push("contractAgreementLog/searchWithFilters");
    endpoints.push("gaiaxlogs/getall");
    endpoints.push("gaiaxlogs/getByDate");
    endpoints.push("gaiaxlogs/searchWithFilters");
    return endpoints;

  }

  private getAliasOfObjectPath<T>(
    objectPath: string,
    connection: Connection,
    entityType: EntityTarget<T>,
    query: SelectQueryBuilder<T>
  ): any {
    const metadata = connection.getMetadata(entityType);
    const path = objectPath.split(".");
    let result: any;
    if (path.length > 1) {
      const initialPath = path[0];
      if (metadata.relations.find((z) => z.propertyName === initialPath)) {
        path.splice(0, 1);
        const foreignKey = metadata.relations.find((z) => z.propertyName === initialPath);
        result = this.getAliasOfObjectPath<T>(path.join("."), connection, foreignKey.type, query);
      }
    } else if (path.length === 1) {
      const alias = query.expressionMap.aliases.find((z) => z.target === entityType);
      result = { alias: alias.name, propertyName: path };
    }

    return result;
  }
}

export const helpers = new Helpers();
