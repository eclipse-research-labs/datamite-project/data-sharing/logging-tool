/**
 * Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 *
 * Contributors:
 * Georgios Nikolaidis - Author
 * Vasileios Siopidis - Coauthor
 * Konstantinos Votis - Coauthor
 */

// import * as socketio from "socket.io";
// // import profileService from "../services/profile.service";
// //import { SocketEvents, SocketUser } from "../types/socket.types";
// import { SERVER_PORT } from "./config.util";
// import { logError, logger } from "./logger.util";

// class SocketUtil {
//   public io: socketio.Server;
//   //private authenticatedUsers: SocketUser[] = [];

//   init(server: any) {
//     this.io = require("socket.io")(server, {
//       cors: {
//         origin: "*"
//       }
//     });

//     // this.io.on(SocketEvents.Connection, socket => {
//     //   socket.on(SocketEvents.Disconnect, () => {
//     //     // if (this.authenticatedUsers.find(x => x.socket.id === socket.id)) {
//     //     //   this.authenticatedUsers.splice(
//     //     //     this.authenticatedUsers.findIndex(x => x.socket.id === socket.id),
//     //     //     1
//     //     //   );
//     //     // }
//     //   });

//     //   socket.on(SocketEvents.Authorize, async (data: any) => {
//     //     try {
//     //       // if (data.token) {
//     //       //   const decoded = await tokenUtil.verifyToken(data.token);
//     //       //   if (!this.authenticatedUsers.find(x => x.socket.id === socket.id)) {
//     //       //     const profile = await profileService.getTokenProfile(decoded.userId);
//     //       //     const socketUser = new SocketUser(profile, socket);
//     //       //     this.authenticatedUsers.push(socketUser);
//     //       //   }
//     //       // }
//     //     } catch (err) {
//     //       logError(err);
//     //     }
//     //   });
//     // });

//     logger.info(`Sockets initialized successfully on port: ${SERVER_PORT}`);
//   }

//   getSocketUserByProfileId(profileId: number) {
//     //return this.authenticatedUsers.find(x => x.profile.id === profileId);
//   }
// }

// export default new SocketUtil();
