/**
 * Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * SPDX-License-Identifier: MIT
 * 
 * Contributors:
 * Georgios Nikolaidis - Author
 * Vasileios Siopidis - Coauthor
 * Konstantinos Votis - Coauthor
 */

import { getConnection, createConnection } from "typeorm";
// import { Role } from "../models/role.model";
// import { User } from "../models/user.model";

import { logger } from "./logger.util";
import * as bcrypt from "bcrypt-nodejs";


class DbUtil {
  /**
   * @param connectionName The name of the connections. Defaults to defaultDb
   * @returns A Connection from connection manager
   */
  async getDefaultConnection(connectionName: string = "defaultDb") {
    return await getConnection(connectionName);
  }

  /**
   * Initializes the database connections
   */
  async init() {
    logger.info("Creating connection to database");
    logger.info("__dirname :" + __dirname + "./../migrations/*.js");
    logger.info("model :" + __dirname + "./../**/*.model.js");
    try {
      await createConnection({
        name: "defaultDb",
        timezone: "Z",
        type: "mysql",
        host: process.env.DB_HOST,
        port: Number(process.env.DB_PORT),
        username: process.env.DB_USER,
        password: process.env.DB_PASS || null,
        database: process.env.DB_NAME,
        entities: [__dirname + "../../**/*.model.js"],
        migrations: [__dirname + "../../migrations/*.js"],
        // subscribers: [__dirname + "./../subscribers/*.js"],
        synchronize: true,
        logging: false,
        migrationsRun: false,
      });
    } catch (err) { console.log(err); }


    await this.checkInitSeed();
  }

  private async checkInitSeed() {
    const connection = await this.getDefaultConnection();

    // create default roles
    // let roles = await connection.getRepository(Role).find();
    // if (roles.length === 0) {
    //   const roleSuperAdmin = new Role();
    //   roleSuperAdmin.key = RoleTypes.RoleSuperAdmin;
    //   roleSuperAdmin.name = RoleTypes.RoleSuperAdmin;

    //   await connection.manager.save(roleSuperAdmin);

    //   const hourlyStaff = new Role();
    //   hourlyStaff.key = RoleTypes.HourlyStaff;
    //   hourlyStaff.name = RoleTypes.HourlyStaff;

    //   await connection.manager.save(hourlyStaff);

    //   const permanentStaff = new Role();
    //   permanentStaff.key = RoleTypes.PermanentStaff;
    //   permanentStaff.name = RoleTypes.PermanentStaff;

    //   await connection.manager.save(permanentStaff);

    //   const academy = new Role();
    //   academy.key = RoleTypes.Academy;
    //   academy.name = RoleTypes.Academy;

    //   await connection.manager.save(academy);


    // }




  }
}

export default new DbUtil();
