/**
 * Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 *
 * Contributors:
 * Georgios Nikolaidis - Author
 * Vasileios Siopidis - Coauthor
 * Konstantinos Votis - Coauthor
 */

// import passport from "passport";
// import { Request, Response, NextFunction } from "express";
// import { userService } from "../services/user.service";
// import passportBearer from "passport-http-bearer";
// // tslint:disable-next-line: variable-name
// const BearerStrategy = passportBearer.Strategy;
// import { HTTPStatusCode } from "../types/HttpStatusCode";
// import * as Enumerable from "linq";
// import { tokenUtil } from "./token.util";
// import { RoleTypes } from "./config.util";
// import { User } from "../models/user.model";

// const options: any = {
//   usernameField: "email"
// };

// const apiOptions: any = {
//   passReqToCallback: true,
//   session: false
// };

// passport.use(
//   new BearerStrategy(apiOptions, async (req: any, token: any, done: any) => {
//     try {
//       const decoded: any = await tokenUtil.verifyToken(token);

//       // verify also jwtid
//       if (decoded.jti !== "JWTID" + decoded.iat.toString(26).toUpperCase()) {
//         return done(undefined, false, { name: "JsonWebTokenError", message: `invalid token` });
//       }

//       userService.getUserByEmail(decoded.sub).then(
//         (user: User) => {
//           if (!user) {
//             return done(undefined, false, { message: `Email ${decoded.sub} not found.` });
//           }

//           if (!user.hasRole(RoleTypes.RoleSuperAdmin)) {
//             user.userRoles = user.userRoles.filter(x => x.companyId === decoded.companyId);
//           }
//           user.profileId = Number(decoded.profileId);

//           return done(undefined, user);
//         },
//         (err: any) => {
//           return done(err);
//         }
//       );
//     } catch (err) {
//       return done(undefined, false, err);
//     }
//   })
// );

// export let authorizeApi = (roles: string[] = []) => {
//   return (req: Request, res: Response, next: any) => {
//     if (req.method === "OPTIONS") return next();

//     passport.authenticate("bearer", apiOptions, (error: any, user: any, info: any) => {
//       if (error) {
//         return next(error);
//       }

//       if (!user) {
//         res.status(HTTPStatusCode.UNAUTHORIZED);
//         return res.json().send();
//       } else {
//         if (roles.length > 0 && !user.hasAnyRole(roles)) {
//           res.status(HTTPStatusCode.UNAUTHORIZED);
//           return res.json().send();
//         }
//         req.user = user;
//         next();
//       }
//     })(req, res, next);
//   };
// };

// export let isAuthenticated = (req: Request, res: Response, next: NextFunction) => {
//   passport.authenticate("local")(req, res, next);
// };
