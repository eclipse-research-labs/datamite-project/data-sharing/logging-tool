/**
 * Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * SPDX-License-Identifier: MIT
 * 
 * Contributors:
 * Georgios Nikolaidis - Author
 * Vasileios Siopidis - Coauthor
 * Konstantinos Votis - Coauthor
 */

import * as express from "express";
import { logError } from "./logger.util";

export async function handle(app: express.Application) {
  function parseStackTrace(stackTrace: string) {
    let stack: string[] = [];
    if (stackTrace) {
      stack = stackTrace.split("\n");

      for (let i = 0; i < stack.length; i++) {
        stack[i] = stack[i].trim();
      }
    }
    return stack;
  }
  app.use(async (err: any, req: any, res: any, next: any) => {
    logError(err);
    if (process.env.NODE_ENV === "development") {
      return res.status(500).json({ message: err.message, stack: parseStackTrace(err.stack) });
    } else {
      return res.status(500).json({ message: "" });
    }
  });
  app.use(async (req: any, res: any, next: any) => {
    res.status(404);

    if (req.accepts("json")) {
      res.json({ message: "The url was not found" });
      return;
    }
  });
}
