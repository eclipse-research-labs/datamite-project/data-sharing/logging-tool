/**
 * Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * SPDX-License-Identifier: MIT
 * 
 * Contributors:
 * Georgios Nikolaidis - Author
 * Vasileios Siopidis - Coauthor
 * Konstantinos Votis - Coauthor
 */

import * as dotenv from "dotenv";
import * as fs from "fs";
import { Keycloak } from 'keycloak-backend';

if (fs.existsSync(".env")) {
  dotenv.config({ path: ".env" });
}

export const BASE_ASSET_FOLDER = String(process.env.BASE_ASSET_FOLDER);
export const EMAIL = String(process.env.EMAIL);
export const SITE_URL = String(process.env.SITE_URL);

export const SMTP_HOST = String(process.env.SMTP_HOST);
export const SMTP_PORT = Number(process.env.SMTP_PORT);
export const SMTP_USE_TLS = Number(process.env.SMTP_USE_TLS) === 0 ? false : true;
export const SMTP_USER_NAME = String(process.env.SMTP_USER_NAME);
export const SMTP_USER_PASSWORD = String(process.env.SMTP_USER_PASSWORD);
export const SMTP_FROM = String(process.env.SMTP_FROM);
export const FRONT_URL = String(process.env.FRONT_URL);
export const ADDRESS = String(process.env.ADDRESS);
export const PHONE = String(process.env.PHONE);

export const TOKEN_EXPIRES_IN_ACCESS = Number(process.env.TOKEN_EXPIRES_IN_ACCESS);
export const TOKEN_EXPIRES_IN_REFRESH = Number(process.env.TOKEN_EXPIRES_IN_REFRESH);

export const SEND_EMAILS = Number(process.env.SEND_EMAILS) === 1 ? true : false;

export const NODE_ENV = String(process.env.NODE_ENV);
export const HASH_SECRET = String(process.env.hashSecret);

export const TAXIS_AUTH = String(process.env.TAXIS_AUTH);
export const TAXIS_CLIENT_ID = String(process.env.TAXIS_CLIENT_ID);
export const TAXIS_CLIENT_SECRET = String(process.env.TAXIS_CLIENT_SECRET);
export const TAXIS_REDIRECT_URI = String(process.env.TAXIS_REDIRECT_URI);
export const TAXIS_FRONT_SUCCESS = String(process.env.TAXIS_FRONT_SUCCESS);
export const TAXIS_FRONT_FAIL = String(process.env.TAXIS_FRONT_FAIL);

export const clientId = String(process.env.clientId);
export const clientSecret = String(process.env.clientSecret);
export const oauth2serverUserInfoURL = String(process.env.oauth2serverUserInfoURL);
export const accessTokenUri = String(process.env.accessTokenUri);
export const userAuthorizationUri = String(process.env.userAuthorizationUri);
export const redirectUri = String(process.env.redirectUri);

export const KEYCLOAK_URL = String(process.env.Keycloak_url);



