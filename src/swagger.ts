/**
 * Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * SPDX-License-Identifier: MIT
 */
import swaggerAutogen from 'swagger-autogen';
import fs from "fs";
import path from "path";

const doc = {
    info: {
        version: 'v1.0.0',
        title: 'Swagger Logging tool',
        description: 'Implementation of Swagger with TypeScript'
    },
    servers: [
        {
            url: 'http://localhost:29788',
            description: 'Logging tool api docs'
        },
    ],
    schemes: ["http"],
    components: {
        securitySchemes: {
            bearerAuth: {
                type: 'http',
                scheme: 'bearer',
            }
        }
    }
};

const outputFile = './swagger_output.json';

const endpointsFiles = ['./src/routes/index.ts', './src/routes/actionLog.route.ts', './src/routes/assetLog.route.ts', './src/routes/policyLog.route.ts',
    './src/routes/contractLog.route.ts', './src/routes/metadataLog.route.ts'];

// files.filter(x => x.indexOf(".route") > -1 && x.indexOf(".map") === -1).forEach(x => {
//     points.push(x);
//     endpointsFiles.push(x);
// });




swaggerAutogen({ openapi: '3.0.1' })(outputFile, endpointsFiles, doc);
