User Guide
====

 **Getting Started**
----

Describe how end-users can access and use the component.


 **UI Walkthrough**
----

Provide a step-by-step guide with screenshots of the user interface.