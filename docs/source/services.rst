Services
=====



The webservice of the logging tool is operating on https://datamite.api.iti.gr/docs as part of the Datamite project.

On the below link there is a detail documentation of the provided services using the Swagger documentation

 `Swagger documentation <https://datamite.api.iti.gr/docs/>`_.




.. image:: https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-sharing/logging-tool/-/raw/main/logging/images/assets.png?ref_type=heads

.. image:: https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-sharing/logging-tool/-/raw/main/logging/images/policies.png?ref_type=heads

.. image:: https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-sharing/logging-tool/-/raw/main/logging/images/assets.png?ref_type=heads






