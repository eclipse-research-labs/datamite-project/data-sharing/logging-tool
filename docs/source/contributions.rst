Contributions & Community
====

 **Contribution Guidelines**
----

Provide instructions on how others can contribute.


  **Reporting Issues & Feature Requests**
----

Explain how to submit bug reports or feature suggestions.